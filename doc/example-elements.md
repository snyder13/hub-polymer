# Example Components

## Goal
To demonstrate how to use hub-polymer, we'll make two simple components: a data source and a visualization. Having both will allow us to test communication between the two:

![Elements usage example](img/poly-example.gif)

## Setup
These completed components already exist in the repo, under `src/components/examples/function-generator` and `src/components/examples/line-chart`. If you would like to play along, move or delete these folders.

You should have already completed the setup instructions in the main README.md file, and be able to run the dev server.

## Scaffolding
There is a script that generates a basic component from the template in `src/skeleton`:

    $ node scripts/skeleton examples/function-generator
    src/components/examples/function-generator/index.js
    src/components/examples/function-generator/style.pcss
    src/components/examples/function-generator/template.html
    added import to src/index.js

    $ node scripts/skeleton examples/line-chart
    src/components/examples/line-chart/index.js
    src/components/examples/line-chart/style.pcss
    src/components/examples/line-chart/template.html
    added import to src/index.js

Every element gets its own folder, which may have related sub-elements in it. Slashes are changed to dashes and `hub-` is prepended to make the element name, so here we defined `<hub-examples-function-generator>` and `<hub-examples-line-chart>`. 

Using `node scripts/skeleton examples-function-generator` would have defined an element with the same name, but placed its folder at the components root instead of in the examples folder. It's a judgment call depending on how you'd like to organize things.

With the dev server running (`$ yarn dev:module`) you'll be able to see these components pop up in the generated documentation at `http://localhost:3000`:

![Documentation output](img/skeleton-doc.png)

You can go directly to an element you're working on by clicking its name, or adding it to the query string: `http://localhost:3000/?el=hub-examples-function-generator`. This helps in development to show you what your examples look like automatically when you change the code.

While not terribly interesting, it may be useful to look at the files at this stage to see how this trivial example works.

### File structure
#### .../*element*/index.js
This is the main definition for the component's behavior. It extends the `HubElement` class, which provides some common behaviors on top of Polymer's `Element` base class. The purposes of this file are to:

* Define which properties and events the component uses to derive and communicate its state
* Respond to lifecycle events where appropriate. E.g., do something in `ready()` to set up
* Render an HTML representation of the state from the `template()` getter
* Implement whatever other utility methods are needed for it to operate
* Provide metadata about events and properties for the purposes of generated documentation

More about all these when we fill them in below.

#### .../*element*/style.pcss
[PostCSS](https://github.com/postcss/postcss) style for the component. You can think of this exactly like plain CSS, as it doesn't currently use any plugins that significantly change that. It does add vendor prefixes for you so you can specify only the base tags for things like `box-shadow` that would otherwise have spotty implementation.

You can also use Sass, LESS, etc, if you add the appropriate Webpack plugin for them.

#### .../*element*/template.html
This is a plain HTML template that operates similarly to a React JSX template. One-way binding is accomplished with `[[brackets]]` referring to a property name, while two-way binding occurs through `{{braces}}` on elements that are capable of mutating that property.

Control structures in the template are accomplished by using subcomponents that implement them, namely `<dom-if>` and `<dom-each>`.

Like the PostCSS setup, this is just a simple default. You can use other markup languages so long as it gets compiled down to a return of HTML from the `template()` getter.

#### src/index.js
The delivery strategy for these elements is to lump them all into one bundle. This helps to reduce duplication of common libraries like D3, and to reduce the number of imports needed in the code by making everything available.

The skeleton adds `import`s to this file for new elements you create so they're included in the next build.

## Implementation
### The data source, `<hub-examples-function-generator>`
Per the image example above, we just want to generate numbers over an interval according to a few simple functions.

#### Properties
We can define the properties of this component like so:
```javascript
static get properties() {
	return {
		'function': {
			'type': String,
			'description': 'sin, cos or random'
		},
		'start': {
			'type': Number,
			'value': 0,
			'description': 'start at (degrees)'
		},
		'end': {
			'type': Number,
			'value': 360,
			'description': 'end at (degrees)'
		},
		'step': {
			'type': Number,
			'value': 1,
			'description': 'sample frequency (degrees)'
		}
	};
}
```
We don't actually mess with `start`, `end`, or `step` for the purposes of the demo, and they could be defined as constants. It's often nicer to pull out arbitrary choices into properties though, so the caller could adjust if they wanted, e.g., `<hub-examples-function-generator start=0 end=100 step=5>`. We put our defaults in the `value` attribute, and the properties get mapped to instance variables. (`this.end` is 360, or 100 with the previous example specifying it).

The description field is an optional extension that is used by the documentation generator.

`function` indicates which type of generator we are using.

#### Implementation
There is a `ready()` method called by Polymer when the element is added to a document, and we could generate values based on the properties there as one-off operation. To improve interactivity, though, it'd be nice to automatically regenerate the values whenever the properties change. To do this, we can add observers:
```javascript
static get observers() {
	return [
		'_recompute(function, start, end, step)'
	];
}
```

The observer listens for any of the property changes that could impact its result, and in this case we listen to all of them.

Then, the generator for our values:

```javascript
_recompute(fun, start, end, step) {
	// must specify a function, rest have defaults so we don't have to check
	if (!fun) {
		return;
	}

	// calculate
	this.value = [];
	for (let x = start; x <= end; x += step) {
		this.value.push([ x, Math[fun](x * (Math.PI/180)) ])
	}

	// export data
	this.dispatchEvent(new CustomEvent('change', {
		'detail': { 'value': this.value },
		'composed': true
	}));
}
```

(The underscore in the method name is just a convention indicating private methods.)

After generating an array of values we emit an event describing it. You can dispatch any sort of events you want, but `change` in particular is special because of its treatment in the `HubElement` base class. If an element has its `id` attribute set, and it sends out a `change` event, `HubElement` will record it in a global store under that identifier. Then, other elements can bind to that identifier to be notified about the event. This is how we'll make the visualization element react to changes to this generator element.

There are a few more familiar ways to read data out of elements which correspond to common JavaScript idiom:

* Look at the object properties or DOM attributes of the element:
	```
	const test = document.getElementsByTagName('hub-function-generator')[0];
	console.log(test.value, test.getAttribute('value'));
	 ```
	This is fine when you want a snapshot of the data at a given time, like when the user clicks a "Run" button.

* Listen for events:
	```
	test.addEventListener('change', ({ detail }) => console.log(detail.value));
	```
	This allows reactive responses to cascade across the page automatically when properties update. Note that by default, events do not pass the boundary of the web component sub-document they are spawned from (the "shadow DOM"). The `'composed': true` property we specified above when dispatching changes this behavior.

Finishing up here, there is an HTML-returning method defined by the skeleton template you don't have to worry about unless you want to do something special. It just concatenates the CSS and HTML files associated with the element

```javascript
static get template() {
	return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
}
```
Then there are a few optional methods that are used to generate further documentation about the element:
```javascript
// usage examples for doc page
static get examples() {
	return {
		'basic usage (set function prop for default)': '<hub-examples-function-generator function="cos"></hub-examples-function-generator>'
	};
}

// dispatched event descriptions for doc page
static get events() {
	return {
		'change': 'Provides new value as Event.detail.value'
	};
}
```

If an example emits a `change` event its `detail` will be noted on the documentation page.

#### Template
To make the demo interactive, we'll add a few buttons to change the function type in `template.html`
```html
<div id="prnt">
	<hub-input-select id="function-select" value="{{function}}">
		<hub-input-button class="active" name="sin">Sine</hub-input-button>
		<hub-input-button name="cos">Cosine</hub-input-button>
		<hub-input-button name="random">Random</hub-input-button>
	</hub-input-select>
</div>
```
The value of the `function` property is bound to `value` in the select element with a two-way binding. If the select's value changes, that value will be reflected in the property, and likewise if the property's value changes (e.g., something external running `funcGen.function = 'random'`), the select's value will be updated to reflect as much.

This binding is enough to get this working, we don't need to add event listeners or anything. The buttons will be very large if we don't constrain them by their parent element though, so in `style.pcss`:

```css
#prnt {
	width: 100px;
}
```

#### Testing
Putting this all together, we can consult `http://localhost:3000` again and click through the options to verify it works:
![documentation output](img/funcgen-doc.png)

### The visualization, `<hub-examples-line-chart>`

#### Properties
```javascript
static get properties() {
	return {
		'data': {
			'type': Array,
			'value': () => [],
			'observer': '_dataChanged',
			'description': 'Data to plot, should be an array of [x, y] coordinates'
		},
		// used to not show an empty space in the shape of the svg when there's no data to draw on it
		'visibility-class': {
			'computed': '_computeVisibilityClass(data)'
		}
	};
}
_computeVisibilityClass(data) {
	return data.length > 0 ? 'visible' : 'hidden';
}
```

You can define the `value` of a property using a callback. This is useful if you can figure it out dynamically from context, or, as in this case, any time you use the `Object` or `Array` types. Because `Object`s and `Array`s are passed by reference in JavaScript, it is necessary to use this so that each instance of the element gets its own new array. `'value': []` would result in multiple elements on the page all sharing a reference to the same array.

If there is an observer with behavior that only depends on on property, you can define it more succinctly like it's done here for `_dataChanged`. We use this observer in the same way we use the observer in the function generator, to reactively change state according to incoming updates.

`visibility-class` is here because Polymer templates don't allow you to do anything more advanced that `!`negating a property. So you can't write, for example, `<foo style="display: [[data.length > 0 ? 'block' : 'none']]"`. The computed property returns a class name that we can interpolate directly with '[[visibility-class]]', and updates it whenever its precedents change. Computed properties can have more than one property to respond to, like observers.

For demonstration purposes we'll adapt an existing D3 visualization example to do the heavy lifting here.

At the top of the page with the other imports, bring in d3:
```javascript
import * as d3 from 'd3';
```
Webpack imports are capable of finding things in your `node_modules/` path, so you don't have to be exact about where it is.

Then, the observer:
```javascript
// adapted from https://bl.ocks.org/mbostock/3883245
_dataChanged(data) {
	if (this.update) {
		return this.update(data);
	}
	const svg = this.svg = d3.select(this.$.prnt);

	const margin = { 'top': 20, 'right': 20, 'bottom': 30, 'left': 50};
	const width = +svg.attr('width') - margin.left - margin.right;
	const height = +svg.attr('height') - margin.top - margin.bottom;
	const g = svg.append('g').attr('transform', `translate(${ margin.left }, ${ margin.top })`);

	const x = d3.scaleLinear()
		.rangeRound([ 0, width ]);
	const y = d3.scaleLinear()
		.rangeRound([ height, 0 ]);

	// removed tsv loading of data from example, we're using our data property and letting the caller arrange its contents
	// example had x mapped to d.date and y to d.close, since we have a plain array x is d[0] and y is d[1]
	const line = d3.line()
		.x((d) => x(d[0]))
		.y((d) => y(d[1]));

	x.domain(d3.extent(data, (d) => d[0]));
	y.domain(d3.extent(data, (d) => d[1]));

	// dropped axis label since we don't have anything useful for it
	g.append('g')
		.attr('class', 'x-axis')
		.attr('transform', `translate(0, ${ height })`)
		.call(d3.axisBottom(x))

	g.append('g')
		.attr('class', 'y-axis')
		.call(d3.axisLeft(y))

	g.append('path')
		.datum(data)
		.attr('class', 'line')
		.attr('fill', 'none')
		.attr('stroke', 'steelblue')
		.attr('stroke-linejoin', 'round')
		.attr('stroke-linecap', 'round')
		.attr('stroke-width', 1.5)
		.attr('d', line);

	// make a closure over the relevant variables to animate updates rather than redrawing from scratch
	this.update = (data) => {
		x.domain(d3.extent(data, (d) => d[0]));
		y.domain(d3.extent(data, (d) => d[1]));
		const trans = svg.transition().duration(this.animate);
		trans.select('.line').attr('d', line(data));
		trans.select('.x-axis').call(d3.axisBottom(x));
		trans.select('.y-axis').call(d3.axisLeft(y));
	};
}

```

The D3 example was modified to use the `data` passed to the observer rather than loading from an external file, and an `update` mode was added to support nice transitions between data sets.

To finish up, we can skip `events()` since none are dispatched here, and add a few examples:
```javascript
// usage examples for doc page
static get examples() {
	return {
		'basic usage': '<hub-examples-line-chart data="[[0,1], [1, 2], [2, 3], [3, 2], [4, 1]]"></hub-examples-line-chart>',
		'data bind through hub-element':
`<hub-examples-function-generator id="fg"></hub-examples-function-generator>
<hub-examples-line-chart data-bind="fg"></hub-examples-line-chart>`
	};
}
```

#### Template

Just drop an SVG in `template.html` for the D3 to target:
```javascript
<svg class$="[[visibility-class]]" id="prnt" width="960" height="500"></svg>
```
If an element has a `id=foo`, it is available in the class as `this.$.foo`.

The dollar sign in the class binding has to do with the subtle difference between property and attribute binding. The dollar sign says that we specifically want to set the DOM attribute class, which is actually called `className` in its property form. We want to set the attribute instead because that's what CSS selectors use to match class names. We can use a one-way binding here because `visibility-class` is computed and it doesn't make sense to try to push any change back to it.

The purpose of all this is to avoid having a blank space allocated for a chart with no data assigned to it. We can complete the task with `style.pcss`:

```css
.hidden {
	display: none;
}
```

#### Testing
Check out the documentation again, scrolling to this element's examples:

![documentation examples](img/ex-examples.png)

You can also run these example in a Jupyter notebook (setup instructions are in the root README), in which case the bound data will be stored in a Python variable with a name matching the identifier:

![Jupyter example](img/examples-notebook.png)

In order to keep to notebook conventions, this configuration is not reactive. After the data is changed the graph will not update to reflect it until its cell is re-evaluated. Elements sharing the same cell can be reactive with each other.

The buttons should operate as in the gif at the top of this document.enter image description here. If not, or if you have any other questions or concerns, please [file an issue](https://bitbucket.org/snyder13/hub-polymer/issues)

