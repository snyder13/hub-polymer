import Rusha from 'rusha';
import { IPythonUploader, BlockServerUploader } from './upload-strategies';

const DEFAULT_BLOCK_SIZE = Math.pow(2, 18);

export default class HubApi
{
	constructor(host, id) {
		this.host = host;
		this.id = id;
		this.services = (window.IPython || window._hubExecPython) ? Promise.resolve('notebook') : request('GET', `${ this.fullHost }/service-manifest.json`);
	}

	get fullHost() {
		return (/^http/.test(this.host) ? this.host : `https://${ this.host }`).replace(/\/+$/, '');
	}

	getServiceUrl(type, extra = '') {
		return this.services.then((manifest) => `${ manifest[type] }/${ extra }`);
	}

//	getRappture(tool) {
//		return request('GET', `https://${ this.host }/api/tools/${ tool }/rappturexml`)
//			.then((res) => new Rappture(res.responseXML), console.error);
//	}

	get blockSize() {
		return this.services.then((type) =>
			type === 'notebook' ? { 'bytes': DEFAULT_BLOCK_SIZE } : request('GET', this.getServiceUrl('block-server', 'blocks/size'))
		);
	}

	uploadFile(file, props = {}, progress) {
		const uploader = new IPythonUploader(file);
		uploader.id = this.id;
		uploader.onprogress = progress;
		uploader.upload();
	}

	_uploadBlock(data) {
		if (!this.sha) {
			this.sha = new Rusha;
		}
		return new Promise((resolve, reject) => {
			const fr = new FileReader;
			fr.onload = ({ target }) => {
				const hash = this.sha.digest(target.result);
				return request('HEAD', this.getServiceUrl('block-server', `blocks/${ hash }`))
					.then((res) => resolve(hash))
					.catch((res) => {
						// 404 is fine, just means we need to send the block
						// bubble other errors
						if (res.status !== 404) {
							return reject(res);
						}
						return request('PUT', this.getServiceUrl('block-server', `blocks/${ hash }`), target.result)
							.then(() => resolve(hash));
					});
			};
			fr.readAsArrayBuffer(data);
		});
	}
}


// rappture xml to polymer elements WIP, ignore me
export class Rappture {
	constructor(xml) {
		this.xml = xml;
		const tool = xml.getElementsByTagName('tool');
		if (!tool[0]) {
			return;
		}

		this._setGlobalProps(tool[0]);
		this._setVersionInfo(tool[0]);

		this._setLoaders(tool[0]);
		// load from default loader
		if (!this.load()) {
			// when something is loaded, _setInputs is called automatically
			// otherwise, we ought to do it
			const inp = xml.getElementsByTagName('input');
			if (!inp[0]) {
				return;
			}
			this._setInputs(inp[0]);
		}
		this.debugXML = this.xml.toString().replace(/</g, '&lt;');
		console.log(this);
	}

	get XMLString() {
		return (new XMLSerializer).serializeToString(this.xml);
	}

	load(src) {
		let file = src;
		// can try to load without an argument to init/resume the default, if a
		// default is in fact defined
		if (!src) {
			const loader = this.xml.getElementsByTagName('loader');
			if (!loader[0]) {
				return false;
			}
			if (!(file = childText(loader[0], 'default'))) {
				return false;
			}
		}
		if (!this.loaders[file]) {
			console.error(`no loader ${file} is defined`);
		}
		// copy in <current> from given loader
		[...this.loaders[file].getElementsByTagName('current')].forEach((cur) => {
			const parents = [];
			let t = cur.parentNode;
			while (t.parentNode.tagName !== 'run') {
				// sub-tag <current>s are handled as part of parent's merge
				if (t.tagName === 'current') {
					return;
				}
				parents.unshift({
					'tag': t.tagName,
					'id': t.id
				});
				t = t.parentNode;
			}
			mergeDOM(cur, this.xml.getElementsByTagName('input')[0], parents);
		});
		// update inputs to reflect document changes
		this._setInputs(this.xml.getElementsByTagName('input')[0]);
		return true;
	}

	_setLoaders(tool) {
		this.loaders = {};
		[...this.xml.getElementsByTagName('run')].forEach((doc) => {
			const src = doc.getAttribute('source');
			if (src) {
				this.loaders[src] = doc;
				doc.parentNode.removeChild(doc);
			}
		});
	}

	_setInputs(inp) {
		this.phases = [];
		childTags(inp, 'phase').forEach((phase) =>
			this._setPhase(phase)
		)
	}

	_setGlobalProps(tool) {
		// set some top-level data about the tool
		[ 'id', 'name', 'title', 'about', 'command', 'layout', 'cache' ].forEach((prop) =>
			this[prop] = childText(tool, prop)
		);
	}

	_setVersionInfo(tool) {
		this.version = {};
		const version = childTag(tool, 'version');
		if (!version) {
			return;
		}
		this.versionID = childText(version, 'identifier');
		const app = childTag(version, 'application');
		if (!app) {
			return;
		}
		const isDate = { 'modified': true, 'installed': true };
		[ 'revision', 'modified', 'installed', 'directory' ].forEach((prop) => {
			const val = childText(app, prop);
			this.version[prop] = isDate[prop] ? new Date(val) : val;
		});
	}

	_setPhase(phase) {
		this.phases.push({
			'id': phase.id,
			'global': new InputGroup(phase)
		});
	}
}

class InputGroup
{
	constructor(prnt) {
		this.id = prnt.id;
		this.groups = [];
		this.inputs = [];

		[...prnt.children].forEach((node) => {
			const nodeDef = {
				'type': node.tagName,
				'id': node.id
			};
			switch (node.tagName) {
				case 'integer': case 'number':
					let about = null;
					[...node.children].forEach((child) => {
						if (child.tagName === 'about') {
							about = child;
							return;
						}
						nodeDef[child.tagName] = child.textContent;
					});
					if (about) {
						[...about.children].forEach((child) => {
							nodeDef[child.tagName] = child.textContent;
						});
					}
					nodeDef.enable = nodeDef.enable !== 'no';

					this.inputs.push(nodeDef);
				break;
				case 'group':
					this.groups.push(new InputGroup(node));
				break;
				case 'about':
					[...node.children].forEach((child) =>
						this[child.tagName] = child.textContent);
				break;
				case 'loader': case 'separator': return;
				default: console.log(`no handler defined for input type ${node.tagName}`);
			}
		});
	}
}

function childTags(prnt, tag) {
	return [...prnt.children].filter((child) =>
		child.tagName === tag
	);
}

function childTag(prnt, tag) {
	let rv = null;
	[...prnt.children].some((child) => {
		if (child.tagName === tag) {
			rv = child;
			return true;
		}
	});
	return rv;
}

function childText(prnt, tag) {
	const el = childTag(prnt, tag);
	return el ? el.textContent : null;
}

function mergeDOM(src, dst, lineage) {
	let target = dst;
	lineage.forEach((path) => {
		let el = null;
		if (![...target.children].some((child) => {
			if (path.id && child.id === path.id) {
				target = child;
				return true;
			}
			if (!path.id && child.tagName == path.tag) {
				target = child;
				return true;
			}
		})) {
			console.error('unable to traverse lineage when merging DOM');
			console.log({ path, target });
			return false;
		}
	});
	const ex = target.getElementsByTagName(src.tagName);
	if (!ex[0]) {
		target.appendChild(src);
	}
	else {
		/// @TODO probably not right -- think we have to recursively find/replace
		target.replaceChild(src, ex[0]);
	}
}

function request(mtd, url, params) {
	if (url.then) {
		return url.then((resolved) => request(mtd, resolved, params));
	}
	const req = new XMLHttpRequest();
	if (mtd === 'GET' && params) {
		url += '?' + Object.entries(params).map(([ k, v ]) =>
			`${ encodeURIComponent(k) }=${ encodeURIComponent(v) }`
		);
	}
	req.open(mtd, url, true);

	return new Promise((resolve, reject) => {
		req.onload = () => {
			if (req.status >= 200 && req.status < 300) {
				const ctype = req.getResponseHeader('Content-Type');
				return resolve(req.responseText && /^application\/json/.test(ctype) ? JSON.parse(req.responseText) : req);
			}
			reject(req);
		};
		req.onerror = (err) => {
			reject(err);
		};
		if ((mtd === 'POST' || mtd == 'PUT' || mtd === 'DELETE') && params) {
			if (params.constructor == ArrayBuffer) {
				req.setRequestHeader('Content-Type', 'application/octet-stream');
			}
			else {
				req.setRequestHeader('Content-Type', 'application/json');
				if (typeof(params) !== 'string') {
					params = JSON.stringify(params);
				}
			}
			req.send(params);
		}
		else {
			req.send();
		}
	});
}
