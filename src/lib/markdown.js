'use strict';

import { Converter } from 'showdown';
const conv = new Converter;

export function markdownElement(el) {
	el.innerHTML = conv.makeHtml(el.innerHTML);
};

export function markdownChildren(el) {
	[...el.querySelectorAll('.md')].forEach(markdownElement);
}
