'use strict';
const babel = require('babel-core');
const fs = require('fs');
const compBase = `${ __dirname }/../components`;
const { resolve } = require('path');

const api = module.exports = {
	'list': (base = compBase) => {
		base = resolve(base);
		const packages = api.packages(base);
		Object.values(packages).forEach((desc) => desc.components = []);

		// find all index.js in the component path, which should be 1-1 with elements
		walkDir(base, ({ file }) => file === 'index.js')
			.forEach(({ path, fullPath }) => {
				const relPath = path.replace(`${ base }/`, '');
				let pkg = '';
				Object.keys(packages).forEach((pkgPath) => {
					if ((new RegExp(`^${ pkgPath }`)).test(relPath) && pkgPath.length > pkg.length) {
						pkg = pkgPath;
					}
				});
				if (!packages[pkg]) {
					packages[pkg] = {
						'components': []
					};
				}
				// export all the data about each element that might be useful to generate doc
				packages[pkg].components.push({
					'path': relPath,
					...api.parse(fullPath, base)
				});
			})
		Object.values(packages).forEach((desc) =>
			desc.components = desc.components.sort((a, b) => a.path > b.path ? 1 : -1)
		);
		return packages;
	},
	'parse': (path, base = compBase) => {
		try {
			// get source for later reference. the locations in the AST refer to the pre- and not post-processed source
			const code = fs.readFileSync(path, { 'encoding': 'utf8' });
			// parse file. can't just require() it for a variety of reasons
			const { ast } = babel.transformFileSync(path, {
				'plugins': [ 'transform-object-rest-spread' ]
			});
			// counts on "export default class HubTagName ..."
			const exported = ast.program.body.filter((node) =>
				node.type === 'ExportDefaultDeclaration'
			);
			const clss = exported[0].declaration;
			const rv = {
				'className': clss.id.name,
				// HubTagName -> hub-tag-name
				'tagName'  : clss.id.name.replace(/([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase()
			};
			// these methods (if they exist) should all return an object literal.
			[ 'properties', 'events', 'examples', 'description' ].forEach((mtdName) =>
				rv[mtdName] = getMethodReturn(clss, code, mtdName)
			);
			return rv;
		}
		catch (error) {
			console.error('parsing meta', path, error);
			return { error }
		}
	},
	'packages': (base = compBase) => {
		base = resolve(base);
		const rv = {};
		walkDir(base, ({ file }) => file === 'package.json')
			.forEach(({ path, fullPath }) =>
				rv[resolve(path).replace(`${ base }/`, '')] = require(fullPath)
			);
		return rv;
	}
};

function getMethodReturn(clss, code, mtdName) {
	// avoid bubbling up errors so partial documentation can be generated from the working methods, rather than failing the whole file
	try {
		// find "static get mtdName() {...}"
		const propsMtd = clss.body.body.filter((node) =>
			node.type === 'ClassMethod' && node.key.name === mtdName
		);
		if (propsMtd.length) {
			// expected to return an object literal
			const propsRet = propsMtd[0].body.body.filter((node) =>
				node.type === 'ReturnStatement'
			);
			if (propsRet.length) {
				const propsObj = propsRet[0].argument;
				// clip out the literal and try to evaluate it
				// the 'value' attribute of properties, when it depends on some context, should be defined in an arrow function.
				// e.g., instead of 'value': window.location, 'value': () => window.location
				// this keeps this step from barfing when evaluating the definition since it lacks that context.
				return feval(code.substr(propsObj.start, propsObj.end - propsObj.start));
			}
		}
		return mtdName === 'description' ? null : {};
	}
	catch (error) {
		console.error(`parsing meta`, `${ clss.id.name }.${ mtdName }`, error);
		return { error };
	}
}

function feval(code) {
	return Function(`"use strict"; return (${ code });`)();
}

// recursive dir scan, including those files for which inc({ path, file, stat, fullPath }) is truthy
function walkDir(path, inc, rv = []) {
	fs.readdirSync(path).forEach((file) => {
		const fullPath = `${ path }/${ file }`;
		const stat = fs.statSync(fullPath);
		if (stat.isDirectory()) {
			walkDir(fullPath, inc, rv);
		}
		if (inc({ path, file, stat, fullPath })) {
			rv.push({ path, file, stat, fullPath });
		}
	});
	return rv;
}
