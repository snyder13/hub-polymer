/**
 * Mechanism to bind a component state to an IPython variable
 *
 * The expectations of the implementing component are that it includes a String
 * 'notebook-bind' in its properties, and that it call the exported function with
 * `this` during ready()
 *
 * This call returns a promise that resolves either with the state contained in
 * the IPython variable or null.
 *
 * Additionally, event listeners are attached for the 'ready' and 'change'
 * events, to propogate state back to that variable.
 *
 * When emitting these events, you can set 'state' in the detail to represent
 * what you want saved, or you can implement a 'state' getter to call for it.
 * The former is preferred where available.
 *
 * If your state is not JSON portable since it has cycles or contains functions
 * or what-have-you, you can implement a custom serialization and pass it as
 * detail.serializedState. This must be a string and must JSON.parse() to
 * something.
 */
export default (inst) => {
	if (!inst.constructor.properties['notebook-bind']) {
		// this can still work, so don't throw -- not ideal anyway
		console.warn(`${ inst.constructor.name } should define a String property 'notebook-bind'`);
	}

	const key = inst['notebook-bind'];
	/// @TODO could listen for this to be bound later? nbd
	if (!key) {
		return Promise.resolve(null);
	}
	if (!window.IPython) {
		// the notebook-bind attribute can be copy/pasted into contexts where
		// it isn't appropriate, but it's not a show-stopper
		console.info(`${ inst.constructor.name } can't bind to a notebook because IPython is not defined`);
		return Promise.resolve(null);
	}

	[ 'ready', 'change' ].forEach((evt) => inst.addEventListener(evt, update(key)));
	return restore(key);
};

/**
 * Set the python variable
 *
 * If the event has 'state' in its details, that'll be used.
 *
 * For a custom representation, the component can set 'serializedState', which
 * should be a JSON-parseable string. This method must be used if the state
 * would otherwise not be JSON-portable (cycles, functions, etc)
 *
 * Failing either of those, the component should implement a 'state' getter
 */
function update(key) {
	return (evt) => {
		const state = evt.detail.serializedState
			? evt.detail.serializedState
			: JSON.stringify(evt.detail.state ? evt.detail.state : evt.target.state);
		IPython.notebook.kernel.execute(`${ key } = ${ state }`);
	};
}

/**
 * Restore state from a given python variable
 *
 * Resolves with the value of the Python variable where available or null.
 */
function restore(key) {
	return new Promise((resolve, reject) => IPython.notebook.kernel.execute(
// python code -- try to echo the relevant variable
`import json
json.dumps(${ key })`,
		{
			'iopub': {
				'output': (res) => {
					if (!res.content || !res.content.data || !res.content.data['text/plain']) {
						// variable isn't known to IPython. that's normal
						return resolve(null);
					}
					try {
						return resolve(JSON.parse(res.content.data['text/plain'].replace(/^'|'$/g, '')));
					}
					catch (ex) {
						// most likely scenario for this failure is python code
						// modifying the state variable in an incompatible way
						return reject('failed to restore notebook state', ex);
					}
				}
			}
		},
		{ 'silent': false, 'store_history': false, 'stop_on_error': true }
	));
}
