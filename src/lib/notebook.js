'use strict';
export default class Notebook {
	static eval(code) {
		console.log({ code });
		if (window.IPython) {
			return new Promise((resolve, reject) => {
				IPython.notebook.kernel.execute(
					// print something to ensure the iopub callback is called
					`${ code }`,
					{
						'iopub': {
							'output': ({ msg_type, content }) => {
								if (msg_type === 'error') {
									return reject(content);
								}
								console.log({ content });
								resolve(content.text ? content.text : content.data['text/plain']);
							}
						}
					},
					{ 'silent': false, 'store_history': false, 'stop_on_error': true }
				);
			});
		}
		return _hubExecPython.ready.then((exec) => exec(code)).then((res) => res.output);
	}

	static get ready() {
		return window.IPython ? IPython.promises.notebook_loaded : window._hubExecPython ? _hubExecPython.ready : new Promise(() => { /* never resolves */ });
	}

	static get available() {
		return window.IPython || window._hubExecPython;
	}
}
