'use strict';
import { Element } from '@polymer/polymer/polymer-element';
import Notebook from './notebook';
import { markdownChildren } from './markdown';
const binds = {};

export default class HubElement extends Element {
	ready() {
		super.ready();

		// interpret markdown in anything with class="md"
		setImmediate(() => markdownChildren(this.root));

		// if id is set, attempt to share value
		if (this.id) {
			// through change events for other HTML elements
			this.addEventListener('change', ({ detail }) => {
				if (detail.value && binds[this.id]) {
					binds[this.id].forEach((el) => el.data = detail.value);
				}
			});
			// or, in a Jupyter context, save value to python variable of the name given by id
			Notebook.ready.then(() => this._bindNotebook());
		}

		// if data-bind is set, attempt to connect to another element with that id
		const bind = this.getAttribute('data-bind');
		if (bind) {
			// in jupyter, this means reading from the python variable
			if (Notebook.available) {
				Notebook.ready.then(() => this._getNotebookValue(bind)
					.then((val) => {
						if (val !== null) {
							this.value = val;
						}
						else {
							if (!binds[bind]) {
								binds[bind] = [];
							}
							binds[bind].push(this);
						}
					}));
			}
			// "plain" html method, just register to be notified about change events (above)
			else {
				if (!binds[bind]) {
					binds[bind] = [];
				}
				binds[bind].push(this);
			}
		}
	}

	// sync value attribute with python variable
	_bindNotebook() {
		// some elements emit rapid change events, this avoids flooding the jupyter websocket by not sending faster than latency
		const latency = 200;
		let debounce = null;
		// update python on change
		this.addEventListener('change', ({ detail }) => {
			if (debounce) {
				clearInterval(debounce);
			}
			debounce = setTimeout(() => this._updateNotebookValue(detail.value), latency);
		});

		// when first called, try to resurrect state from python variable if it exists
		return this._getNotebookValue(this.id).then((val) => this.value = val);
	}

	// read a variable from python. uses json transport
	_getNotebookValue(varName) {
		return new Promise((resolve) => Notebook.eval(`import json\njson.dumps(${ varName })`)
			.then((val) => {
				val = val.replace(/^'|'$/g, '').replace(/\\\\/g, '\\');
				resolve(JSON.parse(val));
			}, () => resolve(null))
		);
	}

	// writee a variable to python. uses json transport
	_updateNotebookValue(val) {
		if (val === undefined) {
			return;
		}
		val = JSON.stringify(val).replace(/'/g, '\\\'').replace(/\\"/g, '\\\\"');
		return Notebook.eval(`import json\n${ this.id } = json.loads('${ val }', strict=False)\nprint()`);
	}
}

