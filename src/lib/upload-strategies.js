'use strict';

import Notebook from './notebook';

class Uploader
{
	constructor(file, id) {
		this.file = file;
		this.id = id;
		this.onprogress = () => {};
		this.props = {
			'name': file.name,
			'type': file.type,
			'modified': new Date(file.lastModified).toISOString()
		};
	}

	_begin() {}
	_end() {}

	upload() {
		Promise.resolve(this._begin())
			.then(() => this.blockSize)
			.then((bytes) => {
				let work = Promise.resolve();
				const blockCount = Math.ceil(this.file.size / bytes);
				for (let idx = 0; idx < blockCount; ++idx) {
					work = work
						.then(() => {
							const fr = new FileReader();
							fr[`readAs${ this.blockFormat }`](
								this.file.slice(idx * bytes, idx * bytes + bytes, 'application/octet-stream')
							);
							return new Promise((resolve, reject) =>
								fr.onload = ({ target }) =>
									this._uploadBlock(target.result)
										.then(resolve, reject)
							);
						})
						// is/of = %/100
						.then(() => this.onprogress(((bytes * idx + bytes)*100)/this.file.size))
				}
				return work;
			})
			.then(() => this._end());
	}
}

/**
 * Upload a file to the notebook path by writing chunks of it with base64 transport to/from JS
 *
 * Very slow in my tests, files > ~4M require a lot of patience. Smaller files take a long time to
 * kick off, hence low block size to report first progress earlier.
 */
export class IPythonUploader extends Uploader
{
	get blockFormat() {
		return 'DataURL';
	}

	get blockSize() {
		// larger numbers are slightly faster overall but smaller show initial progress more quickly, ideally be a few mb if the notebook architecture could support it
		return Math.pow(2, 18);
	}

	_begin() {
		if (!this.id) {
			// state is persisted using a user-provided id where available. just try not to clash with any other names accidentally otherwise
			this.id =  `_hub_input_file_${ Math.random().toString(16).replace(/^0./, '') }`;
		}
		return Notebook.ready
			.then(() => Notebook.eval(
// truncate the file
`${ this.id } = { 'state': 'pending', 'fh': open(${ JSON.stringify(this.props.name) }, 'w') }
${ this.id }['fh'].close()
print('')`
			));
	}

	_uploadBlock(dataUrl) {
		return Notebook.eval(
`import base64

if (${ this.id }['state'] == 'pending'):
  ${ this.id }['fh'] = open(${ JSON.stringify(this.props.name) }, 'ab')
  ${ this.id }['state'] = 'writing'

${ this.id }['fh'].write(base64.b64decode("${ dataUrl.substr(dataUrl.indexOf(',') + 1) }"))`
		);
	}

	_end() {
		return Notebook.eval(
// mark finished, re-open a handle for use elsewhere
`${ this.id }['fh'].close();
${ this.id }['fh'] = open(${ JSON.stringify(this.props.name) }, 'r')
${ this.id }['state'] = 'complete'`
		);
	}
}

export class BlockServerUploader extends Uploader
{
	get blockFormat() {
		return 'ArrayBuffer';
	}
}
