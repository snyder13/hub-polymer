'use strict';

/* Import WebpackApp */
import '@polymer/polymer/lib/elements/dom-if';
import '@polymer/polymer/lib/elements/dom-repeat';

/* eslint-disable no-unused-vars */
import './components/tool';
import './components/input';
import './components/input/file';
import './components/input/material-structure';
import './components/ui/progress';
import './components/agent';
import './components/agent/connection';
import './components/input/button';
import './components/agent/argument';
import './components/input/select';
import './components/input/option';
import './components/ui/panel';
import './components/ui/panel/icon';
import './components/agent/web-service';
import './components/agent/data-service';
import './components/agent/security-feedback';
import './components/input/tree-select';
import './components/input/tree-select/level';
import './components/viz/scatter';
import './components/viz/text';
import './components/viz/collection';
import './components/input/unit';
import './components/agent/data-service/input-definition';
import './components/agent/data-service/output-definition';
import './components/agent/input-definition/preset';
import './components/tool/custom-interface';
import './components/tool/input-collection';
import './components/examples/function-generator';
import './components/examples';
import './components/examples/line-chart';
import './components/input/modal';
import './components/input/code';
import './components/examples/pkg';
import './components/ui/tags';
import './components/examples/component';
import './components/viz/geo';
