'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '%SRC_REL%/lib/hub-element';

import css from './style.pcss';
import globalCss from '%SRC_REL%/global-style/global.pcss';
import template from './template.html';

export default class %CLASS_NAME% extends HubElement {
	static get properties() {
		return {
			'attr': {
				'type': String,
				'value': 'hello'
			}
		};
	}

	// usage examples for doc page
	static get examples() {
		return {
			'basic usage': '<%ELEMENT_NAME%></%ELEMENT_NAME%>'
		};
	}

	/*
	// dispatched event descriptions for doc page
	static get events() {
		return {
			'change': 'Provides new value as Event.detail.value'
		};
	}
	*/

	/*
	ready() {
		super.ready();
		// initialization, if any
	}
	*/

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('%ELEMENT_NAME%', %CLASS_NAME%);
