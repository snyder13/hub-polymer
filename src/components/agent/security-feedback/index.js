'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';
import msgpack from 'msgpack5';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

const { encode, decode } = msgpack();
const CSP_POLL_INTERVAL = 5000;

export default class HubAgentSecurityFeedback extends Element {
	static get properties() {
		return {
			'log': {
				'type': Array,
				'observer': '_hider'
			},
			'csp-id': {
				'type': String
			},
			'domain': {
				'type': String
			},
			'service': {
				'type': String
			},
			'last-csp-timestamp': {
				'type': String
			}
		};
	}

	ready() {
		super.ready();
		this._hookDragging();
		this.sock = new WebSocket(`wss://${ location.host }/${ location.pathname }`);
		this.sock.onopen = () => {
			this.sock.onmessage = (evt) => this._handleMessage(evt);
			this.sock.send(encode({
				'action': 'shareProxyEvents',
				'domain': this.domain,
				'service': this.service,
				'log': this.log.map((evt) => {
					evt.url = location.toString();
					return evt;
				}),
				'relay-csp': true,
				'csp-id': this['csp-id']
			}));

			setInterval(() => this.sock.send(encode({
					'action': 'pollCspEvents',
					'since': this['last-csp-timestamp'],
					'csp-id': this['csp-id']
				})),
				CSP_POLL_INTERVAL
			);
		};
	}

	_handleMessage(evt) {
		this._decodeMessage(evt.data).then((msg) => {
			switch (msg.action) {
				case 'cspEvents':
					this._internCspEvents(msg.events);
				break;
				default: console.warn('unhandled event', msg);
			}
		});
	}

	_internCspEvents(events) {
		if (!events.length) {
			return;
		}
		const newEvents = [];
		events.forEach((evt) => {
			const get = (k) => evt['csp-report'] && evt['csp-report'][k] ? evt['csp-report'][k] : `unknown ${ k }`;
			const message = `${ get('effective-directive') } violation (${ get('blocked-uri') }), line ${ get('line-number') }`;
			const log = { message, 'type': 'csp', 'timestamp': (new Date).toISOString(), 'url': location.toString(), 'context': { 'policy': get('original-policy') } };
			console.log(evt);
			newEvents.push(log);
			this.push('log', log);
			this['last-csp-timestamp'] = log.timestamp;
		});

		this.sock.send(encode({
			'action': 'shareProxyEvents',
			'domain': this.domain,
			'service': this.service,
			'log': newEvents,
			'relay-csp': false,
			'csp-id': this['csp-id']
		}));
	}

	_decodeMessage(data) {
		return new Promise((resolve, reject) => {
			const fr = new FileReader;
			fr.onload = (evt) => resolve(decode(evt.target.result));
			fr.readAsArrayBuffer(data);
		});
	}

	_hider() {
		this.$.prnt.style.display = this.log.length ? 'block' : 'none';
	}

	_hookDragging(evt) {
		let dragging = false;
		this.$.prnt.addEventListener('mouseup', () => {
			this.$.prnt.setAttribute('class', this.$.prnt.getAttribute('class').replace(/\s*dragging\s*/g, ''));
			dragging = false;
		});
		this.$.prnt.addEventListener('mousedown', () => {
			this.$.prnt.setAttribute('class', this.$.prnt.getAttribute('class') + ' dragging');
			dragging = true;
		});
		this.$.prnt.addEventListener('mousemove', (evt) => {
			if (!dragging) {
				return;
			}
			this.$.prnt.style.right = 'auto';
			this.$.prnt.style.bottom = 'auto';
			this.$.prnt.style.left = `${ evt.pageX - this.$.prnt.offsetWidth/2 }px`;
			this.$.prnt.style.top  = `${ evt.pageY - this.$.prnt.offsetHeight/2 }px`;
		});
	}

	_logContext(item) {
		return item.context ? JSON.stringify(item.context) : '';
	}

	_toggleContext(evt) {
		evt.stopPropagation();
		const ctx = evt.path[1].querySelector('.context');
		ctx.style.display = ctx.offsetWidth <= 0 && ctx.offsetHeight <= 0 ? 'block' : 'none';
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-agent-security-feedback', HubAgentSecurityFeedback);
