'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';

import css from './style.pcss';
import globalCss from '../../../../global-style/global.pcss';
import template from './template.html';

export default class HubAgentInputDefinitionPreset extends Element {
	static get properties() {
		return {
			'default': {
				'type': Boolean,
				'value': false,
				'observer': '_defaultChanged'
			},
			'default-class': {
				'type': String,
				'computed': '_computeDefaultClass(default)'
			},
			'val': {
				'type': String
			},
			'description': {
				'type': String
			}
		};
	}

	_defaultChanged(def) {
		this.dispatchEvent(new CustomEvent('defaultChanged', {
			'detail': { 'default': def },
			'composed': true,
			'bubbles': true
		}));
	}

	_computeDefaultClass(def) {
		return def ? 'is-default' : '';
	}

	get value() {
		return {
			'default': this.default,
			'value': parseFloat(this.val),
			'label': this.label
		}
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-agent-input-definition-preset', HubAgentInputDefinitionPreset);
