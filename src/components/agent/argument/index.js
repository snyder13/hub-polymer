'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubAgentArgument extends Element {
	static get properties() {
		return {
			'attr': {
				'type': String,
				'value': 'hello'
			}
		};
	}

	/*
	ready() {
		super.ready();
		// initialization, if any
	}
	*/

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-agent-argument', HubAgentArgument);
