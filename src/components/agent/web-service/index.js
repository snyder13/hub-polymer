'use strict';

import { html } from '@polymer/polymer/polymer-element';
import { WsElement } from '../';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';
import 'materialize-css/dist/js/materialize.js';

export default class HubAgentWebService extends WsElement {
	static get properties() {
		return {
			'attr': {
				'type': String,
				'value': 'hello'
			},
			'service-url': {
				'type': String
			},
			'title': {
				'type': String,
				'value': ''
			},
			'username': {
				'type': String
			},
			// 0-5, controls shadow amount, lower is less/closer
			'depth': {
				'type': Number,
				'value': 1
			},
			'access-url': {
				'type': String,
				'computed': '_computeAccessUrl(name, username)'
			},
			'name': {
				'type': String,
				'computed': '_computeName(title)'
			},
			'complete': {
				'type': Boolean,
				'computed': '_computeComplete(name, service-url)'
			},
			'description': {
				'type': String,
				'computed': '_computeDescription(title)'
			},
			'test-status': {
				'type': String,
				'value': 'untested'
			},
			'test-message': {
				'type': String
			}
		};
	}

	ready() {
		super.ready();
		this._hookRemoveButton();
		this._hookTestButton();
	}

	_hookTestButton() {
		this.$.test.addEventListener('click', () => {
			if (this.$.test.disabled) {
				return;
			}
			this['test-status'] = 'in-progress';
			this['test-message'] = 'Testing...';
			this.wsCall('testWebService', {
				'name': this.name,
				'service-url': this.$['service-url'].value
			}, {
				'webServiceStatus': (msg) => {
					const isWarning = msg.status && (msg.status < 200 || msg.status >= 300);
					this['test-status'] = msg.error ? 'error' : isWarning ? 'warning' : 'ok';
					this['test-message'] = msg.error ? msg.error : isWarning ? `Connected, but test request returned non-ok status ${ msg.status }.` : 'Looks good!';
				}
			});
		});
	}

	_isTestOk(testStatus) {
		return testStatus === 'ok';
	}

	_computeDescription(title) {
		return title ? `the service "${ title }"` : 'this service';
	}

	_computeComplete(name, svcUrl) {
		return name && svcUrl;
	}

	_computeName(title) {
		return title.toLowerCase().replace(/[^-a-z0-9]/g, '-').replace(/^[-\s]+|[-\s]+$/g, '').replace(/-{2,}/g, '-');
	}

	_computeAccessUrl(name, username) {
		return name ? `https://da.rmer.info/~${ username }/services/${ name }` : '';
	}

	_hookRemoveButton() {
		const conf = M.Modal.init(this.$['confirm-remove']);
		const remove = () => this.$.prnt.parentNode.removeChild(this.$.prnt);
		this.$.remove.addEventListener('click', () => {
			if (!this.title && !this['service-url']) {
				return remove();
			}
			conf.open();
		});
		this.$['remove-confirmed'].addEventListener('click', remove);
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-agent-web-service', HubAgentWebService);
