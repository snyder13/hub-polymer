'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';
import msgpack from 'msgpack5';
import { v4 as uuid } from 'uuid';

import css from './style.pcss';
import globalCss from '../../global-style/global.pcss';
import template from './template.html';

const { encode, decode } = msgpack();

export class WsElement extends Element {
	wsCall(action, detail, callbacks = {}) {
		detail.action = action;
		detail._callbacks = callbacks;
		if (Object.keys(callbacks).length > 0) {
			detail.refId = uuid();
		}
		this.dispatchEvent(new CustomEvent('ws', {
			detail,
			'bubbles': true,
			'composed': true
		}));
	}
}

export default class HubAgent extends Element {
	static get properties() {
		return {
			'sid': {
				'type': String
			},
			'username': {
				'type': String
			}
		};
	}

	ready() {
		super.ready();
		this.conns = {};
		this.wsCallbacks = {};
		this.sock = new WebSocket(`wss://${ location.host }/${ location.pathname }`);
		this.sock.onopen = () => {
			this.sock.onmessage = (evt) => this._handleMessage(evt);
			this.sock.send(encode({
				'action': 'connect',
				'sid': this.sid
			}));
		};
	}

	_handleMessage(evt) {
		const actions = {
			'getHostDescription': true,
			'disconnect': true
		};
		this._decodeMessage(evt.data).then(({ action, ...ctx }) => {
			if (ctx.refId && this.wsCallbacks[ctx.refId]) {
				if (this.wsCallbacks[ctx.refId][action]) {
					this.wsCallbacks[ctx.refId][action](ctx);
				}
				else {
					console.warn('ignoring ref\'d response', action, ctx);
				}
				return;
			}
			if (!actions[action]) {
				console.error('unhandled action', { action, ctx });
				return;
			}
			this[`_on${ action[0].toUpperCase() }${ action.slice(1) }`](ctx);
		});
	}

	_onDisconnect({ hostname }) {
		this.conns[hostname].status = 'inactive';
	}

	_onGetHostDescription({ hostname, os }) {
		const conn = this.conns[hostname] = document.createElement('hub-agent-connection');
		conn.username = this.username;
		conn.addEventListener('ws', (evt) => {
			if (evt.detail._callbacks) {
				this.wsCallbacks[evt.detail.refId] = evt.detail._callbacks;
				delete evt.detail._callbacks;
			}
			evt.detail.sid = this.sid;
			this.sock.send(encode(evt.detail));
		});
		conn.hostname = hostname;
		conn.platform = os.platform;
		conn.release = os.release;
		const li = document.createElement('li');
		li.appendChild(conn);
		this.$.connections.appendChild(li);
	}

	_decodeMessage(data) {
		return new Promise((resolve, reject) => {
			const fr = new FileReader;
			fr.onload = (evt) => resolve(decode(evt.target.result));
			fr.readAsArrayBuffer(data);
		});
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-agent', HubAgent);
