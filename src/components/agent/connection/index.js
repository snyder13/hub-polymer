'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubAgentConnection extends Element {
	static get properties() {
		return {
			'hostname': {
				'type': String
			},
			'platform': {
				'type': String
			},
			'release': {
				'type': String
			},
			'username': {
				'type': String
			},
			'status': {
				'type': String,
				'value': 'active'
			},
			'status-color': {
				'type': String,
				'computed': '_computeStatusColor(status)',
				'observer': '_statusColorChanged'
			}
		};
	}

	_statusColorChanged(color) {
		this.$.banner.style.borderColor = color;
	}

	_computeStatusColor(status) {
		return status === 'active' ? '#5c8' : '#999';
	}

	ready() {
		super.ready();
		[ 'web', 'data' ].forEach((type) =>
			this.$[`add-${ type }-service`].addEventListener('click', () => {
				const li = document.createElement('li');
				const svc = document.createElement(`hub-agent-${ type }-service`);
				svc.depth = 3;
				svc.username = this.username;
				li.appendChild(svc);
				this.$.services.appendChild(li);
			}));
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-agent-connection', HubAgentConnection);
