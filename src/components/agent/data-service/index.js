'use strict';

import { html } from '@polymer/polymer/polymer-element';
import { WsElement } from '../';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubAgentDataService extends WsElement {
	static get properties() {
		return {
			'attr': {
				'type': String,
				'value': 'hello'
			},
			'title': {
				'type': String,
				'value': ''
			},
			'name': {
				'type': String,
				'computed': '_computeName(title)'
			},
			'username': {
				'type': String
			},
			'command': {
				'type': String,
				'value': ''
			},
			'access-url': {
				'type': String,
				'computed': '_computeAccessUrl(name, username)'
			},
			// 0-5, controls shadow amount, lower is less/closer
			'depth': {
				'type': Number,
				'value': 1
			},
			'complete': {
				'type': Boolean,
				'computed': '_computeComplete(name, command)'
			},
			'description': {
				'type': String,
				'computed': '_computeDescription(title)'
			},
			'test-status': {
				'type': String,
				'value': 'untested'
			},
			'test-message': {
				'type': String
			}
		};
	}

	ready() {
		super.ready();
		this._hookRemoveButton();
		this._hookTestButton();
		this._hookAddInputButton();
		this._hookOutputDeliverySelector();
		this._hookAddOutputButton();
	}

	get definition() {
		const rv = {
			'title': this.title,
			'inputs': [],
			'outputs': []
		};
		const inps = this.$.inputs.getElementsByTagName('hub-agent-data-service-input-definition');
		for (let idx = 0; idx < inps.length; ++idx) {
			const val = inps[idx].value;
			if (val.label) {
				rv.inputs.push(val);
			}
		}
		/// @TODO
		rv.outputs.push(this.$.delivery.value[0] === 'json' ? { 'label': 'Result', 'type': '2d-coordinates' } : { 'label': 'Result', 'type': 'text' });
		return rv;
	}

	_hookOutputDeliverySelector() {
		this.$.delivery.addEventListener('change', ({ detail }) => {
			this.$['outputs-cont'].style.display = detail.value[0] === 'json' ? 'block' : 'none';
			this.$['about-one-document'].style.display = detail.value[1] === 'one document' ? 'block' : 'none';
			this.$['about-newline-separated'].style.display = detail.value[1] === 'newline-separated' ? 'block' : 'none';
		});
	}

	_hookAddOutputButton() {
		this.$['add-output'].addEventListener('click', () => {
			const li = document.createElement('li');
			const out = document.createElement('hub-agent-data-service-output-definition');
			li.appendChild(out);
			this.$.outputs.appendChild(li);
		});
	}

	_hookAddInputButton() {
		this.$['add-input'].addEventListener('click', () => {
			const li = document.createElement('li');
			const inp = document.createElement('hub-agent-data-service-input-definition');
			li.appendChild(inp);
			this.$.inputs.appendChild(li);
		});
	}

	_computeName(title) {
		return title.toLowerCase().replace(/[^-a-z0-9]/g, '-').replace(/^[-\s]+|[-\s]+$/g, '').replace(/-{2,}/g, '-');
	}

	_computeComplete(name, command) {
		return name && command;
	}

	_computeDescription(title) {
		return title ? `the service "${ title }"` : 'this service';
	}

	_hookTestButton() {
		this.$.test.addEventListener('click', () => {
			if (this.$.test.disabled) {
				return;
			}
			this['test-status'] = 'in-progress';
			this['test-message'] = 'Testing...';
			this.wsCall('testDataService', {
				'name': this.name,
				'command': this.command,
				'definition': this.definition
			}, {
				'dataServiceStatus': (msg) => {
					console.log('dss', { msg });
//					const isWarning = msg.status && (msg.status < 200 || msg.status >= 300);
					const isWarning = !msg.error && !msg.output; 
					this['test-status'] = msg.error ? 'error' : isWarning ? 'warning' : 'ok';
					this['test-message'] = msg.error ? msg.error : isWarning ? `Not all parameters had defaults, check the interface to test.` : 'Looks good!';
				}
			});
		});
	}

	_hookRemoveButton() {
		const conf = M.Modal.init(this.$['confirm-remove']);
		const remove = () => this.$.prnt.parentNode.removeChild(this.$.prnt);
		this.$.remove.addEventListener('click', () => {
			if (!this.title /* && !this['service-url'] */) {
				return remove();
			}
			conf.open();
		});
		this.$['remove-confirmed'].addEventListener('click', remove);
	}

	_isTestOk(testStatus) {
		return testStatus === 'ok' || testStatus === 'warning';
	}

	_computeAccessUrl(name, username) {
		return name ? `https://da.rmer.info/~${ username }/services/${ name }` : '';
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-agent-data-service', HubAgentDataService);
