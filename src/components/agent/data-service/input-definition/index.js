'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';

import css from './style.pcss';
import globalCss from '../../../../global-style/global.pcss';
import template from './template.html';

export default class HubAgentDataServiceInputDefinition extends Element {
	static get properties() {
		return {
			'name': {
				'type': String
			},
			'description': {
				'type': String
			},
			'min': {
				'type': Number
			},
			'max': {
				'type': Number
			},
			'domain-relevant': {
				'type': Boolean,
				'value': false
			}
		};
	}


	get value() {
		const unit = this.$.type.value;
		unit.shift();
		const rv = {
			unit,
			'label': this.name,
			'type': [ 'number', 'decimal' ], /// @TODO
			'presets': []
		};
		if (this['domain-relevant']) {
			if (this.min) {
				rv.min = parseFloat(this.min);
			}
			if (this.max) {
				rv.max = parseFloat(this.max);
			}
		}
		const presets = this.$.presets.getElementsByTagName('hub-agent-input-definition-preset');
		for (let idx = 0; idx < presets.length; ++idx) {
			const val = presets[idx].value;
			if (val) {
				rv.presets.push(val);
			}
		}
		return rv;
	}

	ready() {
		super.ready();

		this.$.type.addEventListener('change', (evt) => {
			evt.stopPropagation();
			this['domain-relevant'] = evt.detail.value[0] === 'number';
		});

		this.$['add-preset'].addEventListener('click', () => {
			const li = document.createElement('li');
			const el = document.createElement('hub-agent-input-definition-preset');
			li.appendChild(el);
			this.$.presets.appendChild(li);
		});
		this.$.presets.addEventListener('defaultChanged', (evt) => {
			if (!evt.detail.default) {
				return;
			}
			evt.stopPropagation();
			const presets = this.$.presets.getElementsByTagName('hub-agent-input-definition-preset');
			for (let idx = 0; idx < presets.length; ++idx) {
				if (presets[idx] !== evt.target) {
					presets[idx].default = false;
				}
			}
		});
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-agent-data-service-input-definition', HubAgentDataServiceInputDefinition);
