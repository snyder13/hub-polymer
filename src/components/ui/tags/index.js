'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../../lib/hub-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubUiTags extends HubElement {
	static get properties() {
		return {
			'value': {
				'type': Array
			}
		};
	}

	// usage examples for doc page
	static get examples() {
		return {
			'basic usage': '<hub-ui-tags></hub-ui-tags>'
		};
	}

	/*
	// dispatched event descriptions for doc page
	static get events() {
		return {
			'change': 'Provides new value as Event.detail.value'
		};
	}
	*/

	/*
	ready() {
		super.ready();
		// initialization, if any
	}
	*/

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-ui-tags', HubUiTags);
