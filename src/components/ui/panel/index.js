'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';
import 'materialize-css/dist/js/materialize.js';

export default class HubUiPanel extends Element {
	static get properties() {
		return {
			'title': {
				'type': String
			},
			'icon': {
				'type': String
			},
			'icon-color': {
				'type': String,
				'value': '#666'
			},
			// 0-5, controls shadow amount, lower is less/closer
			'depth': {
				'type': Number,
				'value': 1
			},
			'cta': {
				'type': Boolean,
				'value': false
			}
		};
	}

	className(icon, cta) {
		const classes = [];
		if (cta) {
			classes.push('cta');
		}
		if (icon) {
			classes.push('with-icon');
		}
		return classes.join(' ');
	}

	ready() {
		super.ready();
		if (this.cta) {
			this.$.prnt.style.background = this['icon-color'];
			Waves.attach(this.$.prnt);
		}
	}

	_getComputedIconColor(cta, col) {
		return cta ? '#fff' : col;
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-ui-panel', HubUiPanel);
