'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';

import css from './style.pcss';
import globalCss from '../../../../global-style/global.pcss';
import template from './template.html';

export default class HubUiPanelIcon extends Element {
	static get properties() {
		return {
			'content': {
				'type': String
			},
			'color': {
				'type': String,
				'value': '#666',
				'observer': '_colorChanged'
			}
		};
	}

	_colorChanged(color) {
		this.$.content.style.color = color;
	}

	/*
	ready() {
		super.ready();
		// initialization, if any
	}
	*/

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-ui-panel-icon', HubUiPanelIcon);
