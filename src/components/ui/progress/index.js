'use strict';
/**
 * Attributes:
 *  @TODO
 */

import { Element, html } from '@polymer/polymer/polymer-element';
import '@polymer/polymer/lib/elements/dom-if';

import css from './style.pcss';
import template from './template.html';

export default class HubUiProgress extends Element {
	ready() {
		super.ready();
	}

	static get properties() {
		return {
			'percentage': {
				'type': Number,
				'value': 0,
				'observer': '_percentageChanged'
			},
			'label': {
				'type': String
			},
			'detail': {
				'type': String
			}
		};
	}

	_percentageChanged() {
		this.$.inner.style.width = `${ this.percentage }%`;
		this.$.inner.style.visibility = this.percentage ? 'visible' : 'hidden';
	}

	static get template() {
		return html([`<style>${css}</style> ${template}`]);
	}
}

window.customElements.define('hub-ui-progress', HubUiProgress);
