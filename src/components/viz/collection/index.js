'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';
import 'materialize-css/dist/js/materialize';
import prettyBytes from 'pretty-bytes';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubVizCollection extends Element {
	static get properties() {
		return {
			'data': {
				'type': String,
				'value': ''
			},
			'parsed-data': {
				'type': Array,
				'computed': '_computeParsedData(data)',
				'observer': '_redraw'
			},
			'type': {
				'type': String,
				'value': 'text'
			},
			'title': {
				'type': String,
				'value': 'Output'
			},
			'tool-name': {
				'type': String
			},
			'class-name': {
				'type': String,
				'computed': '_computeClassName(data)'
			},
			'components': {
				'type': Array,
				'computed': '_computeComponents(type)'
			},
			'iter-components': {
				'type': Array,
				'computed': '_computeIterComponents(components)'
			},
			'download-class': {
				'type': String,
				'computed': '_computeDownloadClass(components)'
			},
			'download-size': {
				'type': String,
				'computed': '_computeDownloadSize(data)'
			}
		};
	}

	_computeClassName(data) {
		return data ? 'show' : 'hide';
	}

	_computeParsedData(data) {
		try {
			return JSON.parse(data);
		}
		catch (ex) {
			console.log(data);
			return JSON.parse(`[ ${ data.split(/[\r\n]+/).filter((line) => line.length).join(', ') } ]`);
		}
	}

	_computeDownloadClass(components) {
		return components.length === 0 ? 'download-only' : '';
	}

	_computeDownloadSize(data) {
		return prettyBytes(data.length);
	}

	_computeIterComponents(components) {
		return components.map((name, idx) => {
			return { name, 'class': idx === 0 ? 'active' : '' };
		});
	}

	_computeComponents(type) {
		switch (type) {
			case 'text':
				return [ 'text' ];
			case '2d-coordinates':
				return [ 'scatter', 'text' ];
			case 'binary':
				return [];
			default:
				console.warn(`unhandled data type ${ type }`);
				return [];
		}
	}

	ready() {
		super.ready();
		this.$.tabs.querySelectorAll('.tab').forEach((tab) => Waves.attach(tab));
		this.$.tabs.addEventListener('click', (evt) => {
			if (evt.target.id === 'download') {
				return this._download();
			}
			if (evt.target.tagName !== 'A') {
				return;
			}
			const active = evt.target.parentNode.parentNode.querySelector('.active');
			if (!active || active === evt.target) {
				return;
			}
			active.className = active.classList.remove('active');
			evt.target.parentNode.classList.add('active');
			this._body(evt.target.getAttribute('data-name'));
		});
		this.bodies = {};
		this._body(this.components[0]);
	}

	_body(type) {
		this.$.body.querySelectorAll('.body').forEach((el) => el.style.display = 'none');
		if (!this.bodies[type]) {
			this.bodies[type] = document.createElement(`hub-viz-${ type }`);
			this.bodies[type].classList.add('body');
			this.bodies[type].data = this['parsed-data'];
			this.$.body.appendChild(this.bodies[type]);
		}
		this.bodies[type].style.display = 'block';
	}

	_redraw(data) {
		if (!this.bodies) {
			return;
		}
		Object.keys(this.bodies).forEach((k) => this.bodies[k].data = data);
	}

	_download() {
		const evt = new MouseEvent('click', {
			'view': window,
			'bubbles': false,
			'cancelable': true
		});

		const a = document.createElement('a');
		a.download = `${ this['tool-name'] ? this['tool-name'] + ' ' : '' }${ this.title } ${ (new Date).toISOString() }.json`;
		a.href = `data:application/octet-stream,${ JSON.stringify(this['parsed-data'], null, '\t') }`;
		a.target = '_blank';
		a.dispatchEvent(evt);
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-viz-collection', HubVizCollection);
