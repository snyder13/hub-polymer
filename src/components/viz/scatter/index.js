'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';
import * as d3 from 'd3';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubVizScatter extends Element {
	static get properties() {
		return {
			'width': {
				'type': Number,
				'value': 900
			},
			'height': {
				'type': Number
			},
			'data': {
				'type': Array,
				'value': () => [],
				'observer': '_redraw'
			},
			'normalized-data': {
				'type': Array,
				'computed': '_computeNormalizedData(data)'
			},
			'axis-label-x': {
				'type': String
			},
			'axis-label-Y': {
				'type': String
			}
		};
	}

	_computeNormalizedData(data) {
		const rv = [];
		const mean = {
			'x': 0,
			'y': 0
		};
		data.forEach((point) => {
			const norm = {};
			const vals = Object.values(point);
			norm.x = vals[0];
			norm.y = vals[1];
			rv.push(norm);
			mean.x += norm.x;
			mean.y += norm.y;
		});

		mean.x /= rv.length;
		mean.y /= rv.length;

		// calculate coefficients
		const r = {
			'x': 0,
			'y': 0
		};
		let term1 = 0;
		let term2 = 0;
		rv.forEach(({x, y}) => {
			r.x = x - mean.x;
			r.y = y - mean.y;
			term1 += r.x * r.y;
			term2 += r.x * r.x;
		});

		const b1 = term1 / term2;
		const b0 = mean.y - (b1 * mean.x);

		return rv.map(({x, y}) => {
			return { x, y, 'yhat': b0 + (x * b1) };
		});
	}

	ready() {
		super.ready();
	}

	_redraw() {
		console.log(this.data, this['normalized-data']);
		if (!this['normalized-data'].length) {
			return;
		}
		const margin = {
			'top': 20,
			'right': 30,
			'bottom': 50,
			'left': 60
		};
		const width = this.width - margin.left - margin.right;
		const height = Math.round(this.height ? this.height : this.width * (9/16)) - margin.top - margin.bottom;

		const x = d3.scaleLinear().range([ 0, width ]);
		const y = d3.scaleLinear().range([ height, 0 ]);

		d3.select(this.$.prnt).selectAll('*').remove();
		const svg = d3.select(this.$.prnt)
			.attr('width', width + margin.left + margin.right)
			.attr('height', height + margin.top + margin.bottom)
			.append('g')
			.attr('transform', `translate(${ margin.left }, ${ margin.top })`);

		const line = d3.line()
        .x((d) => x(d.x))
        .y((d) => y(d.yhat));

		x.domain(d3.extent(this['normalized-data'], (d) => d.x));
		y.domain(d3.extent(this['normalized-data'], (d) => d.y));

		svg.append('g')
			.attr('class', 'x axis')
			.attr('transform', `translate(0, ${ height })`)
			.call(d3.axisBottom().scale(x))
			.append('text')
			.attr('class', 'label')
			.attr('x', width)
			.attr('y', -6)
			.style('text-anchor', 'end')
			.text(this['axis-label-x'] ? this['axis-label-x'] : Object.keys(this.data[0])[0]);

		svg.append('g')
			.attr('class', 'y axis')
			.call(d3.axisLeft().scale(y))
			.append('text')
			.attr('class', 'label')
			.attr('transform', 'rotate(-90)')
			.attr('y', 6)
			.attr('dy', '.71em')
			.style('text-anchor', 'end')
			.text(this['axis-label-y'] ? this['axis-label-y'] : Object.keys(this.data[0])[1]);

		svg.selectAll('.dot')
			.data(this['normalized-data'])
			.enter()
				.append('circle')
				.attr('class', 'dot')
				.attr('r', 3)
				.attr('cx', (d) => x(d.x))
				.attr('cy', (d) => y(d.y))
				.append('svg:title')
					.text((d) => `${ d.x },\n${ d.y }`);

		svg.append('path')
			.datum(this['normalized-data'])
			.attr('class', 'line')
			.attr('d', line);
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-viz-scatter', HubVizScatter);
