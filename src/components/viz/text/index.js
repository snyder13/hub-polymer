'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';
import CodeMirror from 'codemirror/lib/codemirror';
import 'codemirror/mode/javascript/javascript';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubVizText extends Element {
	static get properties() {
		return {
			'data': {
				'type': Array,
				'value': () => []
			},
			'highlight': {
				'type': String,
				'value': 'auto'
			},
			'width': {
				'type': Number,
				'value': 900
			},
			'height': {
				'type': Number
			},
			'indent': {
				'type': Number,
				'value': 4
			},
			'formatted-data': {
				'type': String,
				'computed': '_computeFormattedData(data, indent)',
				'observer': '_redraw'
			}
		};
	}

	_computeFormattedData(data, indent) {
		return JSON.stringify(data, null, indent)
	}

	ready() {
		super.ready();

	}
	_redraw(data) {
		this.$.prnt.innerHTML = '';
		const ta = document.createElement('textarea');
		ta.textContent = data;
		this.$.prnt.appendChild(ta);
		/// @TODO respect this.highlight
		CodeMirror.fromTextArea(ta, {
			'mode': 'javascript',
			'theme': 'duotone-light',
			'readOnly': true,
			'lineNumbers': true
		});
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-viz-text', HubVizText);
