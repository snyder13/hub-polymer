'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../../lib/hub-element';
import * as d3 from 'd3';
import * as versor from 'versor/build/versor.min';
import * as d3Inertia from 'd3-inertia';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubVizGeo extends HubElement {
	static get properties() {
		return {
			'features': {
				'type': Array,
				'observer': '_featuresChanged'
			}
		};
	}

	// usage examples for doc page
	static get examples() {
		return {
			'basic usage': '<div style="width: 900px; height: 400px"><hub-viz-geo></hub-viz-geo></div>'
		};
	}

	_featuresChanged(data) {
		const w = this.$.prnt.clientWidth;
		const h = this.$.prnt.clientHeight;

		this.$.shadow.style.width = `${ h }px`;
		this.$.shadow.style.display = 'block';
		this.$.shadow.style.left = `${ (w - h) / 2 }px`;

		const svg = d3.select(this.$.svg)
			.attr('height', h)
			.attr('width', w);

		const map = svg.append('g');
      const projection = d3
			.geoOrthographic()
			.scale([h / 2])
			.clipAngle(90)
			.translate([w / 2, h / 2])
			.rotate([ 0, -5, 0 ]);

		const path = d3
			.geoPath()
			.projection(projection);

		map.append('defs').append('path')
			.datum({ 'type': 'Sphere' })
			.attr('id', 'sphere')
			.attr('d', path);

		map.append('use')
			.attr('class', 'stroke')
			.attr('xlink:href', '#sphere');

		map.append('use')
			.attr('class', 'fill')
			.attr('xlink:href', '#sphere');

		const graticule = map.append('path')
			.datum(d3.geoGraticule().step([ 10, 10 ]))
			.attr('class', 'graticule')
			.attr('d', path);

		const countries = map
			.selectAll('.country')
			.data(data)
			.enter().append('path')
				.attr('d', path)
				.attr('id', (d, i) => `country-${ d.properties.iso_a3 }`)
				.attr('class', 'country');

		const inertia = d3Inertia.geoInertiaDrag(svg, draw, projection);
		function draw() {
			const rotate = projection.rotate();
			projection.rotate([ rotate[0] + 0.12, -5, 0 ]);
			map.selectAll('path').attr('d', path);
		}
	}

	ready() {
		super.ready();
		d3.json('./assets/data/geo-low.json').then(({ features }) => this.features = features);
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-viz-geo', HubVizGeo);
