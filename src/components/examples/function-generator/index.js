'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../../lib/hub-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubExamplesFunctionGenerator extends HubElement {
	static get description() {
		return 'Generate 2d coordinates using a few simple functions';
	}

	static get properties() {
		return {
			'function': {
				'type': String,
				'description': 'sin, cos or random'
			},
			'start': {
				'type': Number,
				'value': 0,
				'description': 'start at (degrees)'
			},
			'end': {
				'type': Number,
				'value': 360,
				'description': 'end at (degrees)'
			},
			'step': {
				'type': Number,
				'value': 1,
				'description': 'sample frequency (degrees)'
			}
		};
	}

	// usage examples for doc page
	static get examples() {
		return {
			'basic usage (set function prop for default)': '<hub-examples-function-generator function="cos"></hub-examples-function-generator>'
		};
	}

	// dispatched event descriptions for doc page
	static get events() {
		return {
			'change': 'Provides new value as Event.detail.value'
		};
	}

	ready() {
		super.ready();
		// don't allow function-changing event to bubble, since we have our own change event
		this.$['function-select'].addEventListener('change', (evt) => evt.stopPropagation());
	}

	static get observers() {
		return [
			'_recompute(function, start, end, step)'
		];
	}

	_recompute(fun, start, end, step) {
		// must specify a function, rest have defaults so we don't have to check
		if (!fun) {
			return;
		}

		// calculate
		this.value = [];
		for (let x = start; x <= end; x += step) {
			this.value.push([ x, Math[fun](x * (Math.PI/180)) ])
		}

		// export data
		this.dispatchEvent(new CustomEvent('change', {
			'detail': { 'value': this.value },
			'composed': true
		}));
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-examples-function-generator', HubExamplesFunctionGenerator);
