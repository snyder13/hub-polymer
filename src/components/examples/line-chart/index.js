'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../../lib/hub-element';
import * as d3 from 'd3';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubExamplesLineChart extends HubElement {
	static get properties() {
		return {
			'data': {
				'type': Array,
				'value': () => [],
				'observer': '_dataChanged',
				'description': 'Data to plot, should be an array of [x, y] coordinates'
			},
			'animate': {
				'type': Number,
				'value': 300,
				'description': 'Duration of tweening animation when data changes (ms)'
			},
			// used to not show an empty space in the shape of the svg when there's no data to draw on it
			'visibility-class': {
				'computed': '_computeVisibilityClass(data)'
			}
		};
	}

	// usage examples for doc page
	static get examples() {
		return {
			'basic usage': '<hub-examples-line-chart data="[[0,1], [1, 2], [2, 3], [3, 2], [4, 1]]"></hub-examples-line-chart>',
			'data bind through hub-element':
`<hub-examples-function-generator id="fg"></hub-examples-function-generator>
<hub-examples-line-chart data-bind="fg"></hub-examples-line-chart>`
		};
	}

	_computeVisibilityClass(data) {
		return data.length > 0 ? 'visible' : 'hidden';
	}

	// adapted from https://bl.ocks.org/mbostock/3883245
	_dataChanged(data) {
		if (this.update) {
			return this.update(data);
		}
		const svg = this.svg = d3.select(this.$.prnt);

		const margin = { 'top': 20, 'right': 20, 'bottom': 30, 'left': 50};
		const width = +svg.attr('width') - margin.left - margin.right;
		const height = +svg.attr('height') - margin.top - margin.bottom;
		const g = svg.append('g').attr('transform', `translate(${ margin.left }, ${ margin.top })`);

		const x = d3.scaleLinear()
			.rangeRound([ 0, width ]);
		const y = d3.scaleLinear()
			.rangeRound([ height, 0 ]);

		// removed tsv loading of data from example, we're using our data property and letting the caller arrange its contents
		// example had x mapped to d.date and y to d.close, since we have a plain array x is d[0] and y is d[1]
		const line = d3.line()
			.x((d) => x(d[0]))
			.y((d) => y(d[1]));

		x.domain(d3.extent(data, (d) => d[0]));
		y.domain(d3.extent(data, (d) => d[1]));

		// dropped axis label since we don't have anything useful for it
		g.append('g')
			.attr('class', 'x-axis')
      	.attr('transform', `translate(0, ${ height })`)
	      .call(d3.axisBottom(x))

		g.append('g')
			.attr('class', 'y-axis')
	      .call(d3.axisLeft(y))

		g.append('path')
			.datum(data)
			.attr('class', 'line')
			.attr('fill', 'none')
			.attr('stroke', 'steelblue')
			.attr('stroke-linejoin', 'round')
			.attr('stroke-linecap', 'round')
			.attr('stroke-width', 1.5)
			.attr('d', line);

		// make a closure over the relevant variables to animate updates rather than redrawing from scratch
		this.update = (data) => {
			x.domain(d3.extent(data, (d) => d[0]));
			y.domain(d3.extent(data, (d) => d[1]));
			const trans = svg.transition().duration(this.animate);
			trans.select('.line').attr('d', line(data));
			trans.select('.x-axis').call(d3.axisBottom(x));
			trans.select('.y-axis').call(d3.axisLeft(y));
		};
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-examples-line-chart', HubExamplesLineChart);
