'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../lib/hub-element';
import qs from 'query-string';

import css from './style.pcss';
import globalCss from '../../global-style/global.pcss';
import template from './template.html';

const query = qs.parse(location.search);

export default class HubExamples extends HubElement {
	static get description() {
		return 'You\'re lookin\' at it';
	}

	static get properties() {
		return {
			'meta': {
				'type': Array,
				'description': 'description, properties, events and examples harvested by lib/element-meta'
			},
			'packages': {
				'computed': '_computePackages(meta)'
			},
			'show-apps': {
				'type': Boolean,
				'value': false
			},
			'show-core': {
				'type': Boolean,
				'value': true
			},
			'single': {
				'type': String,
				'value': () => query.el ? query.el : null
			},
			'single-props': {
				'type': Object,
				'computed': '_computeSingleProps(single, meta)'
			}
		};
	}

	_computeSingleProps(el, meta) {
		let rv = null;
		Object.entries(meta).some(([ _, { components } ]) => components.some((comp) => {
			if (comp.tagName === el || comp.className === el) {
				rv = comp;
				return true;
			}
		}));
		return rv;
	}

	static get observers() {
		return [
			'_filter(show-apps, show-core)'
		];
	}

	_filter(showApps, showCore) {
		if (!this.packageEls) {
			return;
		}
		const types = [];
		if (showApps) {
			types.push('app');
		}
		if (showCore) {
			types.push('core');
		}
		this.packageEls.forEach((el) => el.filter = { types });
	}

	_computePackages(meta) {
		return Object.values(meta)
			.sort((a, b) => a.name > b.name ? 1 : -1)
			.map((pkg) => {
				if (!pkg.keywords) {
					pkg.keywords = [];
				}
				return pkg;
			});
	}

	ready() {
		super.ready();
		this.packageEls = [];
		if (!this.single) {
			setImmediate(() => {
				this.packageEls = [ ...this.root.querySelector('#packages').getElementsByTagName('hub-examples-package') ];
				this._filter(this['show-apps'], this['show-core']);
			});
		}
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-examples', HubExamples);
