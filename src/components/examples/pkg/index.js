'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../../lib/hub-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubExamplesPackage extends HubElement {
	static get properties() {
		return {
			'name': {
				'type': String
			},
			'version': {
				'type': String
			},
			'description': {
				'type': String
			},
			'components': {
				'type': Array
			},
			'keywords': {
				'type': Array
			},
			'type': {
				'computed': '_computeType(name)'
			},
			'open': {
				'type': Boolean,
				'value': false
			},
			'component-class': {
				'computed': '_computeComponentClass(open)'
			},
			'visibility-class': {
				'computed': '_computeVisibilityClass(visible)'
			},
			'visible': {
				'type': Boolean
			},
			'filter': {
				'type': Object,
				'observer': '_filterChanged'
			},
			'visible-component-count': {
				'computed': '_computeVisibleComponentCount(filter)'
			}
		};
	}

	_computeVisibleComponentCount(filter) {
		return this.components.length;
	}

	_computeVisibilityClass(visible) {
		return visible ? 'appear' : 'hidden';
	}

	_computeComponentClass(open) {
		return open ? 'appear' : 'hidden';
	}

	_filterChanged({ types }) {
		let hidden = false;
		if (!types.includes(this.type)) {
			hidden = true;
		}

		return (this.visible = !hidden);
	}

	_computeType(name) {
		return name.replace(/-.*$/, '');
	}

	ready() {
		super.ready();
		this.$.panel.addEventListener('click', () => this.open = !this.open);
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-examples-package', HubExamplesPackage);
