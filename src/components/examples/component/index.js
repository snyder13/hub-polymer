'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../../lib/hub-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubExamplesComponent extends HubElement {
	static get properties() {
		return {
			'name': {
				'type': String
			},
			'description': {
				'type': String
			},
			'properties': {
				'type': Object
			},
			'events': {
				'type': Object
			},
			'examples': {
				'type': Object
			},
			'single': {
				'type': Boolean,
				'value': false
			},
			'iter-examples': {
				'computed': '_computeIterExamples(examples, example-results, example-events)'
			},
			'iter-properties': {
				'computed': '_computeIterProperties(properties)'
			},
			'iter-events': {
				'computed': '_computeIterEvents(events)'
			},
			'example-results': {
				'type': Object,
				'private': true,
				'value': () => { return {}; }
			},
			'example-events': {
				'type': Object,
				'private': true,
				'value': () => { return {}; }
			}
		};
	}

	_computeIterExamples(examples, results, evts) {
		return Object.entries(examples)
			.map(([ description, markup ]) => {
				const rv = { description, markup };
				if (results[description]) {
					rv.results = true;
				}
				if (evts[description]) {
					rv.events = evts[description];
				}
				return rv;
			});
	}

	_computeIterProperties(props) {
		return Object.entries(props)
			.filter(([ _, props ]) => !props.computed && !props.private)
			.sort(([ a ], [ b ]) => a > b ? 1 : -1)
			.map(([ name, props ]) => {
				return { name, ...props };
			});
	}

	_computeIterEvents(evts) {
		return Object.entries(evts)
			.sort(([ a ], [ b ]) => a > b ? 1 : -1)
			.map(([ name, description ]) => {
				return { name, description };
			});
	}

	_getExampleResult(descr) {
		return this['example-results'][descr];
	}

	_runExample(evt) {
		const bump = () => {
			const ex = this.examples;
			this.set('examples', {});
			this.set('examples', ex);
		}

		const item = evt.model.item;
		const targ = evt.target.parentNode.parentNode.querySelector('.evaluated');
		targ.innerHTML = item.markup;
		this['example-results'][item.description] = true;
		this['example-events'][item.description] = [];
		[...targ.querySelectorAll('*')].forEach((el) => {
			el.addEventListener('change', (ch) => {
				/// @TODO not working as well as I'd like, only shows first event?
//				const el = (ch.path ? ch.path[0] : ch.target).tagName.toLowerCase();
//				this['example-events'][item.description] = { 'type': `${ el }.change`, 'detail': JSON.stringify(ch.detail) };
//				bump();
			});
		});

		bump();
	}

	ready() {
		super.ready();
		if (this.single) {
			setImmediate(() => [ ...this.root.querySelectorAll('.eval-button') ].forEach((el) => el.click()))
		}
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-examples-component', HubExamplesComponent);
