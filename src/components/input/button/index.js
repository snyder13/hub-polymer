'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';
import 'materialize-css/dist/js/materialize';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubInputButton extends Element {
	static get description() {
		return 'A button with a material design waves effect';
	}

	static get properties() {
		return {
			'color': {
				'type': String,
				'value': 'light-blue accent-3',
				'description': 'Coloring according to [materialize-css scheme](https://materializecss.com/color.html). (NB: Probably will change to CSS color)'
			},
			'depth': {
				'private': true,
				'type': Number,
				'value': 1
			},
			'disabled': {
				'type': Boolean,
				'value': false
			},
			'arrow-right': {
				'type': Boolean,
				'value': false
			},
			'arrow-class': {
				'type': String,
				'computed': '_computeArrowClass(arrow-right)'
			},
			'highlight': {
				'type': Boolean,
				'value': false
			},
			'text-class': {
				'type': String,
				'computed': '_computeTextClass(highlight)'
			}
		};
	}

	static get examples() {
		return {
			'basic usage': '<hub-input-button>Label is here</hub-input-button>',
			'disabled': '<hub-input-button disabled>Label is here</hub-input-button>',
			'highlighted': '<hub-input-button highlight>Label is here</hub-input-button>',
			'arrow-right, parent fitting': '<div style="width: 300px"><hub-input-button arrow-right>Label is here</hub-input-button></div>'
		};
	}

	_computeTextClass(highlight) {
		return highlight ? 'highlight' : '';
	}

	_computeArrowClass(right) {
		return right ? 'arrow-right' : '';
	}

	ready() {
		super.ready();
		Waves.attach(this.$.button);
		this.$.button.addEventListener('click', (evt) => {
			if (this.disabled) {
				evt.stopPropagation();
			}
		});
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-input-button', HubInputButton);
