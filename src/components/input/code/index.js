'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../../lib/hub-element';
import CodeMirror from 'codemirror/lib/codemirror';
import 'codemirror/addon/display/autorefresh';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubInputCode extends HubElement {
	static get description() {
		return 'Inline code-editing or display';
	}

	static get properties() {
		return {
			'mode': {
				'type': String,
				'description': 'Dynamically-loaded syntax, see `node_modules/codemirror/mode` for possibilities'
			},
			'read-only': {
				'type': Boolean,
				'value': false
			},
			'line-numbers': {
				'type': Boolean,
				'value': false
			},
			'value': {
				'type': String,
				'observer': '_valueChanged'
			}
		};
	}

	// usage examples for doc page
	static get examples() {
		return {
			'basic usage': '<hub-input-code mode="python">import json\njson.dumps(test)</hub-input-code>'
		};
	}

	// dispatched event descriptions for doc page
	static get events() {
		return {
			'change': 'Provides new value as Event.detail.value'
		};
	}

	ready() {
		super.ready();
		const signal = () => this.dispatchEvent(new CustomEvent('change', {
			'detail': {
				'value': this.value
			},
			'composed': true
		}));
		if (!this.value) {
			const innerText = this.$.slot.assignedNodes().map((node) => node.textContent).join('');
			console.log(this.value, innerText);
			if (innerText) {
				this.value = innerText;
			}
		}
		signal();
		this.cm = CodeMirror.fromTextArea(this.$.code, {
			'mode': 'javascript',
			'theme': 'xq-light',
			'readOnly': this['read-only'],
			'lineNumbers': this['line-numbers'],
			'lineWrapping': true,
			'autoRefresh': true
		});
		this.cm.on('changes', () => {
			this.value = this.cm.getValue();
			signal();
		});
	}

	_valueChanged(value) {
		if (this.cm) {
			this.cm.setValue(this.value);
		}
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-input-code', HubInputCode);
