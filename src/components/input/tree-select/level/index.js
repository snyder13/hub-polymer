'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';
import { microTask} from '@polymer/polymer/lib/utils/async';

import css from './style.pcss';
import globalCss from '../../../../global-style/global.pcss';
import template from './template.html';

export default class HubInputTreeSelectLevel extends Element {
	static get description() {
		return 'Sub-element used from `hub-input-tree-select`\n\n@TODO add `private` to remove whole components like this one from doc';
	}

	static get properties() {
		return {
			'choices': {
				'type': Object,
				'value': () => { return {}; },
				'observer': '_choicesChanged'
			},
			'choice-list': {
				'type': Array,
				'computed': '_computeChoiceList(choices)'
			},
			'color': {
				'type': String,
				'value': 'light-blue-text text-darken-4 light-blue lighten-5'
			},
			'child': {
				'type': Boolean,
				'value': false
			},
			'open': {
				'type': Boolean,
				'value': true
			},
			'class-name': {
				'type': String,
				'computed': '_computeClassName(open, child)'
			}
		};
	}

	_computeClassName(open, child) {
		const classes = [];
		if (open) {
			classes.push('open');
		}
		classes.push(child ? 'child' : 'parent');
		return classes.join(' ');
	}

	_choicesChanged(choices) {
		const lis = [];
		this.$.current.innerHTML = '';
		if (this._nextLevel) {
			this.$.next.removeChild(this._nextLevel);
			this._nextLevel = null;
		}
		this['choice-list'].forEach((item) => {
			const li = document.createElement('li');
			li.className = item.className;
			lis.push(li);
			const el = document.createElement('hub-input-button');
			if (/\btrigger\b/.test(item.className)) {
				this._nodeValue = item.name;
				el.highlight = true;
			}
			el.color = this.color;
			el.textContent = item.name;
			el['arrow-right'] = item.isAdvancing;
			li.appendChild(el);
			this.$.current.appendChild(li);
			li.addEventListener('click', () => {
				const isValueChange = !/\btrigger\b/.test(li.className);
				lis.forEach((li) => {
					li.className = li.className.replace(/\b\s*trigger\s*\b/g, '');
					li.querySelector('hub-input-button').highlight = false;
				});
				li.className += ' trigger';
				li.querySelector('hub-input-button').highlight = true;

				if (this.open) {
					this.$.prnt.className = this.$.prnt.className.replace(/\b\s*open\s*\b/g, '');
				}
				else {
					this.$.prnt.className += ' open';
				}
				this.open = !this.open;
				this._setNext(item.isAdvancing ? item.next : {});
				this._nodeValue = item.name;
				if (isValueChange) {
					this.dispatchEvent(new CustomEvent('change', {
						'detail': {
							'value': this.value
						},
						'bubbles': true,
						'composed': true
					}));
				}
			});
		});
	}

	get value() {
		const inner = this._nextLevel ? this._nextLevel.value : [];
		if (this._nodeValue) {
			inner.unshift(this._nodeValue);
		}
		return inner;
	}

	_setNext(items) {
		if (!this._nextLevel) {
			this._nextLevel = document.createElement('hub-input-tree-select-level');
			this._nextLevel.color = this.color;
			this._nextLevel.child = true;
			this.$.next.appendChild(this._nextLevel);
			this._nextLevel.addEventListener('change', (evt) => {
				evt.detail.value = this.value;
			});
		}
		this._nextLevel.choices = items;
		this._nextLevel.open = true;
	}

	_computeChoiceList(choices) {
		const rv = [];
		Object.keys(choices).forEach((name, idx) => {
			const classes = [];
			if (name === 'label') {
				classes.push('label');
			}
			if (idx === 0) {
				classes.push('trigger');
			}
			const isAdvancing = choices[name] && typeof choices[name] === 'object' && Object.keys(name).length > 0;
			if (isAdvancing) {
				classes.push('advancing');
			}
			else {
				classes.push('terminal');
			}
			rv.push({
				isAdvancing,
				'name': name === 'label' ? choices[name] : name,
				'className': classes.join(' '),
				'next': choices[name]
			});
		});
		return rv;
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-input-tree-select-level', HubInputTreeSelectLevel);


