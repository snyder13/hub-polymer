'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';

import { parser } from './level';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubInputTreeSelect extends Element {
	static get properties() {
		return {
			'choices': {
				'type': Object
			}
		};
	}

	ready() {
		super.ready();
		this.topLevel = document.createElement('hub-input-tree-select-level');
		this.topLevel.choices = this.choices;
		this.$.prnt.appendChild(this.topLevel);
	}

	get value() {
		return this.topLevel.value;
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-input-tree-select', HubInputTreeSelect);
