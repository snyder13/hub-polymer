'use strict';

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../../lib/hub-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubInputModal extends HubElement {
	static get properties() {
		return {
			'confirm': {
				'description': 'Provide a cancel button as well. In the "change" event, "accepted" indicates which button was pressed',
				'type': Boolean,
				'value': false
			},
			'proceed-label': {
				'description': 'Label for proceed/accept/ok button',
				'type': String,
				'value': 'Ok'
			},
			'cancel-label': {
				'description': 'Label for cancel button in "confirm" mode',
				'type': String,
				'value': 'Cancel'
			}
		};
	}

	static get description() {
		return 'Pop-over dialogue containing arbitrary content. Any elements in that content that have a "name" defined and also produce a "value" will be included in the modal\'s "value" and change event.';
	}

	static popup(attrs, html) {
		const el = document.createElement('hub-input-modal');
		Object.entries(attrs).forEach(([k, v]) => el.setAttribute(k, v));
		el.innerHTML = html;
		document.body.appendChild(el);
	}

	// usage examples for doc page
	static get examples() {
		return {
			'basic usage (confirm mode)':
`<hub-input-modal confirm>
	<hub-input name="test" value="42" label="Enter something"></hub-input>
</hub-input-modal>`
		};
	}

	// dispatched event descriptions for doc page
	static get events() {
		return {
			'change': 'Event.detail.value: { name/id: value } pairs for all child elements, preferring name, and { accepted: Boolean }, true if they clicked "Ok" and false if "Cancel"'
		};
	}

	_close(accepted) {
		this.$.outer.classList.add('fade-out-outer');
		this.$.inner.classList.add('fade-out-inner');
		setTimeout(() => {
			this.$.outer.parentNode.removeChild(this.$.outer);
			this.$.inner.parentNode.removeChild(this.$.inner);
		}, 300);

		this.value = {
			accepted,
			...this._formData
		};
		this.dispatchEvent(new CustomEvent('change', {
			'detail': {
				'value': this.value
			},
			'composed': true
		}));
	}

	get _formData() {
		const rv = {};
		this.$.slot.assignedNodes().forEach((node) => {
			if (!node.getAttribute) {
				return;
			}
			let name = node.getAttribute('name');
			if (!name) {
				name = node.getAttribute('id');
			}
			if (!name) {
				return;
			}
			rv[name] = node.value;
		});
		return rv;
	}

	ready() {
		super.ready();
		// async to wait for dom-if to be stamped on the cancel button
		setImmediate(() => {
			this.$.proceed.addEventListener('click', () => this._close(true));
			if (this.confirm) {
				this.root.querySelector('#cancel').addEventListener('click', () => this._close(false));
			}
		});
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-input-modal', HubInputModal);
// for static HubInputModal.popup() interface
window.HubInputModal = HubInputModal;
