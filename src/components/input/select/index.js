'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubInputSelect extends Element {
	static get properties() {
		return {
			'value': {
				'type': String,
				'notify': true,
				'description': 'Currently selected element\'s name'
			}
		};
	}

	static get description() {
		return 'Choose among a set of options with "name" attributes.';
	}

	static get examples() {
		return {
			'basic usage (set value for default)':
`<hub-input-select value="bar">
	<div style="width: 100px">
		<hub-input-button name="foo">Foo</hub-input-button>
		<hub-input-button name="bar">Bar</hub-input-button>
	</div>
</hub-input-select>`
		};
	}

	static get events() {
		return { 'change': '"detail.value" reflects selection. (Also fires on initialization if there is a default selection)' };
	}

	ready() {
		super.ready();
		this._hookClicks();
	}

	_hookClicks() {
		let active = null;
		const activate = (node) => {
			active = node;
			node.style.opacity = 1;
			node.classList.add('active');
			this.value = node.getAttribute('name');
			this.dispatchEvent(new CustomEvent('change', {
				'detail': { 'value': this.value },
				'composed': true
			}));
		};
		const deactivate = () => {
			if (!active) {
				return;
			}
			active.style.opacity = 0.5;
			active.classList.remove('active');
		}
		Object.entries(this._findNamed(this.$.slot.assignedNodes())).forEach(([ name, node ]) => {
			node.style.opacity = 0.5;
			if (name === this.value) {
				activate(node);
			}
			node.addEventListener('click', () => {
				if (node === active) {
					return;
				}
				deactivate();
				activate(node);
			});
		});
	}

	_findNamed(nodes, res = {}) {
		nodes.forEach((node) => {
			if (node.getAttribute && node.getAttribute('name')) {
				res[node.getAttribute('name')] = node;
			}
			if (node.childNodes) {
				this._findNamed(node.childNodes, res);
			}
		});
		return res;
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-input-select', HubInputSelect);
