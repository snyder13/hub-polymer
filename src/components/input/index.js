'use strict';
/**
 * Events:
 * 	change: fires when the input, uhhh, changes. you can also observe $thiselement.value
 */

import { html } from '@polymer/polymer/polymer-element';
import HubElement from '../../lib/hub-element';
import { hsl } from '../../../node_modules/d3-color';
import converter from 'hub-unit-conv';
import allUnits from 'hub-unit-conv/defs/bundles/all';

import css from './style.pcss';
import template from './template.html';

const conv = converter(allUnits);

export default class HubInput extends HubElement {
	static get description() {
		return 'Free-entry text input, with some bells and whistles. Needs more';
	}

	static get properties() {
		return {
			'label': {
				'type': String,
				'description': 'Always-visible description'
			},
			'label-position': {
				'type': String,
				'value': 'above',
				'description': 'above or below'
			},
			'align': {
				'type': String,
				'value': 'left',
				'description': 'Text align, left or center'
			},
			'error': {
				'type': String,
				'description': 'Annotate with error message'
			},
			'info': {
				'type': String,
				'description': 'Annotate with informational message'
			},
			'unit': {
				'type': String
			},
			'value': {
				'type': String,
				'notify': true,
				'reflectToAttribute': true,
				'description': 'Raw value'
			},
			'color': {
				'type': String,
				'value': '#ccc',
				'description': 'Any valid CSS color. Attempts to generate a legible foreground/background combination'
			},
			'placeholder': {
				'type': String,
				'description': 'Text that appears above the input when nothing has been entered yet'
			},
			'comp-placeholder': {
				'type': String,
				'computed': '_computeCompPlaceholder(placeholder, presets)'
			},
			'type': {
				'type': String
			},
			'scalar-unit': {
				'type': String,
				'computed': '_computeScalarUnit(unit)'
			},
			'type-domain': {
				'type': String,
				'computed': '_computeTypeDomain(type)'
			},
			'presets': {
				'type': Array,
				'value': () => []
			},
			'preset-color': {
				'type': String,
				'value': 'light-blue lighten-5 grey-text text-darken-2'
			},
			'preset-active-color': {
				'type': String,
				'value': 'light-blue accent-3 z-depth-3'
			},
			'iter-presets': {
				'type': Array,
				'computed': '_computeIterPresets(presets, preset-color, preset-active-color, scalar-unit)',
				'notify': true
			}
		};
	}

	static get examples() {
		return {
			'basic usage': '<hub-input label="Label" placeholder="Placeholder"></hub-input>',
			'positioning options': '<hub-input label="Label" align="center" label-position="below"></hub-input>',
			'add-on message': '<hub-input error="major problems" info="fill this out"></hub-input>',
			'color (generate dark and light elements from base)': '<hub-input color="#fa0"></hub-input>'
		};
	}

	_computeIterPresets(presets, color, activeColor, defUnit) {
		return presets.map((preset) => {
			preset.color = preset.default ? activeColor : color;
			if (!preset.unit) {
				preset.unit = defUnit;
			}
			return preset;
		});
	}

	_computeCompPlaceholder(placeholder, presets) {
		if (placeholder) {
			return placeholder;
		}
		let rv = '';
		presets.some((preset) => {
			if (preset.default) {
				rv = preset.value;
				return true;
			}
		});
		return rv;
	}

	_computeScalarUnit(unit) {
		if (unit && unit[0] === '[') {
			const list = JSON.parse(unit);
			return list[list.length - 1];
		}
		return unit;
	}

	_computeTypeDomain(type) {
		if (type && type[0] === '[') {
			return JSON.parse(type)[0];
		}
		return null;
	}

	ready() {
		super.ready();

		if (!this.value && this.presets) {
			this.presets.some((preset) => {
				if (preset.default) {
					this.value = preset.value;
					return true;
				}
			});
		}
		if (!this.label && this.id) {
			this.label = this.id;
		}

		const baseColor = hsl(this.color);
		this.$.inp.style.borderBottomColor = baseColor.toString();
		// make a lighter background based on accent color
		baseColor.l = 0.96;
		baseColor.opacity = 0.6;
		this.$.inp.style.backgroundColor = baseColor.toString();

		// bubble change event
		const signalChange = () => this.dispatchEvent(new CustomEvent('change', { 'detail': { 'value': this.value }, 'composed': true }));
		this.$.inp.addEventListener('change', signalChange);

		// helper to change ints with up/down arrows
		/// @TODO inconsistent behavior w/ floats compared to inspector panel
		this.$.inp.addEventListener('keydown', (evt) => {
			if (this['type-domain'] === 'number') {
				if (evt.key.length === 1 && !/^[-.0-9eE]$/.test(evt.key)) {
					evt.preventDefault();
					return false;
				}
				const ival = parseInt(this.value);
				// doesn't look like an int
				if (ival != this.value) {
					return;
				}
				switch (evt.key) {
					case 'ArrowUp'  : this.value = ival + 1; break;
					case 'ArrowDown': this.value = ival - 1; break;
					default: return;
				}
			}
			evt.stopPropagation();
			signalChange();
		});

		const presetButtons = this.$.presets.getElementsByTagName('hub-input-button');
		this.$.presets.addEventListener('click', (evt) => {
			evt.stopPropagation();
			for (let idx = 0; idx < presetButtons.length; ++idx) {
				presetButtons[idx].color = presetButtons[idx] === evt.target ? this['preset-active-color'] : this['preset-color'];
			}
			this.value = evt.target.getAttribute('data-value');
		});

		// wait for a microtask length for the dom-if containing #unit to be stamped, otherwise we can't access it
		setImmediate(() => {
			const unitSel = this.root.querySelector('#unit');
			if (!unitSel) {
				return;
			}
			this.workingUnit = unitSel.parsedUnit;
			unitSel.addEventListener('change', ({ detail }) => {
				if (this.workingUnit.unit !== detail.unit) {
					const newPresets = [];
					this.presets.forEach((preset, idx) => {
						preset.value = conv.from(preset.value, this.workingUnit).to([ this.workingUnit.domain, this.workingUnit.prefix, detail.unit ]).toNumber();
						preset.unit = detail.unit;
						newPresets.push(preset);
					});
					this.set('presets', []);
					setImmediate(() => {
						this.set('presets', newPresets);
					});
					this.workingUnit.unit = detail.unit;
				}
				this.workingUnit.prefix = detail.prefix;
			});
		});
	}

	get floatValue() {
		return parseFloat(this.value);
	}

	static get template() {
		return html([`<style>${css}</style> ${template}`]);
	}
}
window.customElements.define('hub-input', HubInput);
