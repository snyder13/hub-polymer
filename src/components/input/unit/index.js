'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';
import converter from 'hub-unit-conv';
import allUnits from 'hub-unit-conv/defs/bundles/all';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

const conv = converter(allUnits);

export default class HubInputUnit extends Element {
	static get properties() {
		return {
			'unit': {
				'type': String
			},
			'parsed-unit': {
				'type': Object,
				'computed': '_computeParsedUnit(unit)'
			},
			'prefixes': {
				'type': Array,
				'computed': '_computePrefixes(parsed-unit)'
			},
			'units': {
				'type': Array,
				'computed': '_computeUnits(parsed-unit)'
			}
		};
	}

	get parsedUnit() {
		return this['parsed-unit'];
	}

	ready() {
		super.ready();
		const change = (evt) => {
			evt.stopPropagation();
			this.dispatchEvent(new CustomEvent('change', {
				'detail': {
					'prefix': this.$.prefixes.value,
					'unit': this.$.units.value
				},
				'bubbles': true,
				'composed': true
			}));
		};
		this.$.prefixes.addEventListener('change', change)
		this.$.units.addEventListener('change', change)
	}

	_computeUnits({ domain, unit }) {
		return conv.getUnits(domain).map((sym) => {
			return { sym, 'selected': sym === unit };
		});
	}

	_computePrefixes({ prefix }) {
		return Object.keys(conv.getPrefixes()).map((sym) => {
			return { sym, 'selected': sym === prefix };
		});
	}

	_computeParsedUnit(unit) {
		const rv = {
			'domain': null,
			'prefix': null,
			'unit': null
		};
		// simple behavior -- @TODO look up domain based on unit?
		if (unit[0] !== '[') {
			rv.unit = unit;
			return rv;
		}
		unit = JSON.parse(unit);
		if (Array.isArray(unit)) {
			// [ domain, prefix, unit ] or prefix omitted [ domain, unit]
			rv.domain = unit[0];
			rv.prefix = unit.length === 3 ? unit[1] : '';
			rv.unit   = unit.length === 3 ? unit[2] : unit[1];
			return rv;
		}
		// presumably already an object shaped like we expect
		return unit;
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-input-unit', HubInputUnit);
