'use strict';
/**
 * Implementation of Rappture's <structure>/<field> control.
 *
 * Events:
 * 	ready  - fires when the initial configuration has been evaluated. { details: { state }}
 * 	change - fires when an input has changed. { details: { state } }
 *
 * Properties:
 * 	state         - read only representation of input values
 * 	unit          - measurement, shown in label
 * 	axis-label
 * 	ticks         - how many numbers to show in axis
 *    notebook-bind - when used in a Jupyter notebook context, sync state with this Python variable name.
 *                    this is a two-way binding, in that when the cell is evaluated the component
 *                    attempts to resume from that variable, and changes made by the user are reflected
 *                    in it
 */

import { Element, html } from '@polymer/polymer/polymer-element';
import * as d3 from 'd3';
import bindToNotebook from '../../../lib/notebook-bind';
import HubAPI from '../../../lib/hub-api';

import css from './style.pcss';
import template from './template.html';

export default class HubInputMaterialStructure extends Element {
	static get description() {
		return '@TODO outdated w/ new jupyter api';
	}

	static get properties() {
		return {
			'unit': {
				'type': String
			},
			'height': {
				'type': String,
				'value': '400px'
			},
			'axis-label': {
				'type': String,
				'value': 'Value'
			},
			'ticks': {
				'type': Number,
				'value': 5
			},
			'notebook-bind': {
				'type': String
			},
			'hub': {
				'type': String,
				'value': () => location.host
			},
			'tool': {
				'type': String
			}
		};
	}

	static get examples() {
	}

	ready() {
		super.ready();

		this.domain = {
			'min': Infinity,
			'max': -Infinity
		};

		this.$.wrap.style.height = this.height;
		// svg axis gets a little extra height to ensure the first tick's label is visible
		this.$.axis.style.height = `${ parseInt(this.height) + 20 }px`;

		let first = true;
		this.hub = new HubAPI(this.hub);
		if (this.tool) {
			this.hub.getRappture(this.tool);
		}

		// listen to child input changes. fires at least once when loaded, but should work with dynamically-added (or removed) inputs as well
		this.$.slot.addEventListener('slotchange', (evt) => {
			// only interested in the <hub-input> tag
			this.inputs = this.$.slot.assignedNodes().filter((node) => node.tagName === 'HUB-INPUT');
			// when this happens for initial load, check python state
			(first && this['notebook-bind'] ? bindToNotebook(this) : Promise.resolve(null)).then((state) => {
				if (state) {
					this.inputs.forEach((inp) => {
						if (state[inp.name]) {
							inp.value = state[inp.name];
						}
					});
				}
				this._init();
				if (first) {
					first = false;
					this.dispatchEvent(new CustomEvent('ready', { 'detail': { 'state': this.state } }));
				}
			});
		});
	}

	get state() {
		const state = {};
		this.inputs.forEach((inp) => state[inp.name] = inp.floatValue);
		return state;
	}

	_init() {
		this.inputs.forEach((inp) => {
			// force some common style elements so the inputs align to the graph as expected
			inp['label-position'] = 'below';
			inp.align = 'center';
			inp.style.display = 'inline-block';
			inp.style.position = 'relative';

			// listen for changes to update position going forward
			inp.addEventListener('change', (evt) => {
				// if domain changed we need to move everything
				if (this._calculateDomain()) {
					if (this.validDomain) {
						this.inputs.forEach((inp) => this._positionInput(inp));
					}
				}
				// otherwise just adjust the changed input
				else {
					this._positionInput(evt.target);
				}

				this.dispatchEvent(new CustomEvent('change', { 'detail': { 'state': this.state } }));
			});
		});
		this._calculateDomain();

		if (this.validDomain) {
			this.inputs.forEach((inp) => this._positionInput(inp));
		}
	}

	_positionInput(inp) {
		const val = inp.floatValue;
		if (isNaN(val)) {
			return;
		}
		const height = parseInt(this.height);
		const spread = this.domain.max - this.domain.min;
		const prop = 1 - (val - this.domain.min)/spread;
		/// @TODO numbers should be derived from style or props
		inp.style.top = `${(prop * (height - 2)) - 50}px`;
	}

	_calculateDomain() {
		this.validDomain = false;
		let max = -Infinity;
		let min = Infinity;
		this.inputs.forEach((inp) => {
			const val = inp.floatValue;
			if (!isNaN(val)) {
				min = Math.min(min, val);
				max = Math.max(max, val);
			}
		});

		const changed = min !== this.domain.min || max !== this.domain.max;
		this.domain = { min, max };
		this.validDomain = this.domain.min !== Infinity && this.domain.max !== -Infinity;
		if (changed) {
			this._renderAxis();
		}
		return changed;
	}

	_renderAxis() {
		const width = 100;
		const height = parseInt(this.height);

		const svg = d3.select(this.$.axis)
			.attr('width', width)
			.attr('height', height);
		// clear prior
		svg.selectAll('*').remove();

		// map domain to available real estate
		const y = d3.scaleLinear()
			.range([ height, 0 ])
			.domain([ this.domain.min, this.domain.max ]);

		svg.append("g")
			.call(d3.axisLeft(y).ticks(this.ticks))
			/// @TODO derive from state
			.attr('transform', 'translate(89, 5)');

		svg.append('text')
			.attr('transform', 'rotate(-90)')
			/// @TODO derive from state
			.attr('y', 15)
			.attr('x', 0 - (height / 2))
			.attr('dy', '1em')
			.attr('class', 'label')
			.style('text-anchor', 'middle')
			.text(`${this['axis-label']} (${this.unit})`);
	}

	static get template() {
		return html([`<style>${css}</style> ${template}`]);
	}
}
window.customElements.define('hub-input-material-structure', HubInputMaterialStructure);
