'use strict';
/**
 * Attributes:
 *  @TODO
 *
 * Events:
 * 	'filesready': fires when the user selects files from the browser or drops them in the incoming area. doesn't imply they've been uploaded anywhere, but you can read data from evt.detail.files
 */

import { Element, html } from '@polymer/polymer/polymer-element';
import prettyBytes from 'pretty-bytes';

import HubApi from '../../../lib/hub-api';

import css from './style.pcss';
import template from './template.html';


export default class HubInputFile extends Element {
	static get description() {
		return 'Uploader with Jupyter connections, automatic resume and deduplication when using data-node service';
	}

	static get properties() {
		return {
			'label': {
				'type': String,
				'value': 'Drag files here, or click to browse'
			},
			'accept': {
				'type': String,
				'value': '*',
				'description': 'Same as accept for `input type="file"`'
			},
			'multiple': {
				'type': Boolean,
				'value': false
			},
			'error': {
				'type': String
			},
			'services': {
				'type': String,
				'value': () => location.host
			},
			'authorization': {
				'type': String
			},
			'id': {
				'type': String
			}
		};
	}

	static get examples() {
		return {
			'control example, won\'t upload anything': '<hub-input-file error="example error message"></hub-input-file>'
		};
	}

	ready() {
		super.ready();
		this.dataApi = new HubApi(this['services'], this.id);
		this.attachEvents();
	}

	attachEvents() {
		// browse selection
		this.$.files.addEventListener('change', (evt) => this._upload(evt.target.files));

		// drag+drop, cancel default events to signal we want to control the behavior
		[ 'over', 'enter', 'leave' ].forEach((evt) =>
			this.$.parent.addEventListener(
				`drag${evt}`, (evt) => evt.preventDefault()
			)
		);
		// hook drop
		this.$.parent.addEventListener('drop', (evt) => {
			evt.preventDefault();
			this._upload(evt.dataTransfer.files);
		});
	}

	_upload(files) {
		if (files.length > 1 && !this.multiple) {
			this.error = 'One file at a time, please';
			return;
		}
		this.dispatchEvent(new CustomEvent('filesready', { 'detail': { files } }));
		if (!this['services']) {
			return;
		}
		let work = Promise.resolve();
		[...files].forEach((file) => work = work.then(() => {
			const li = document.createElement('li');
			const bar = document.createElement('hub-ui-progress');
			li.appendChild(bar);
			bar.label = file.name
			bar.detail = `${ file.type }, ${ prettyBytes(file.size) }`;
			this.$.progress.appendChild(li);
			return this.dataApi.uploadFile(file, {}, (pct) =>
				bar.percentage = pct
			);
		}));
		return work;
	}

	static get template() {
		return html([`<style>${css}</style> ${template}`]);
	}
}
window.customElements.define('hub-input-file', HubInputFile);
