'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';
import { WsElement } from '../../agent/';
import msgpack from 'msgpack5';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

const { encode, decode } = msgpack();

export default class HubToolCustomInterface extends WsElement {
	static get properties() {
		return {
			'data': {
				'type': String
			},
			'tool': {
				'type': String
			}
		};
	}

	ready() {
		super.ready();
		this.sock = new WebSocket(`wss://${ location.host }/${ location.pathname }`);
		this.$.slot.addEventListener('slotchange', (evt) => {
			const out = this.$.slot.assignedNodes().filter((node) => node.tagName === 'HUB-VIZ-COLLECTION')[0];
			if (this.data) {
				out.data = this.data;
			}
			this.$.slot.assignedNodes().filter((node) => node.tagName === 'HUB-TOOL-INPUT-COLLECTION')[0].addEventListener('run', ({ detail }) => {
				this.sock.send(encode({
					'action': 'runDataService',
					'tool': this.tool,
					'command': this.command,
					'args': detail.args
				}));
				out.data = this._fermi(detail.args);
			});
		});
	}

	_fermi({ T, Ef }) {
		const rv = [];
		const kT   = 8.61734e-5 * T;
		const Emin = Ef - 10*kT;
		const Emax = Ef + 10*kT;
		const dE   = 0.005*(Emax - Emin);
		let E    = Emin;
		let f    = 0.0;

		while (E < Emax) {
			f = 1.0/(1.0 + Math.exp((E - Ef)/kT));
			rv.push(JSON.stringify({ f, E }));
			E = E + dE;
		}
		return rv.join('\n');
	}


	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-tool-custom-interface', HubToolCustomInterface);
