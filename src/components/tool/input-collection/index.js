'use strict';

import { Element, html } from '@polymer/polymer/polymer-element';

import css from './style.pcss';
import globalCss from '../../../global-style/global.pcss';
import template from './template.html';

export default class HubToolInputCollection extends Element {
	static get properties() {
		return {
			'attr': {
				'type': String,
				'value': 'hello'
			}
		};
	}

	ready() {
		super.ready();
		this.$.run.addEventListener('click', () => {
			const args = {};
			this.$.slot.assignedNodes().filter((node) => node.tagName === 'HUB-INPUT').forEach((node) => args[node.name] = node.value);
			this.dispatchEvent(new CustomEvent('run', {
				'detail': { args },
				'bubbles': true,
				'composed': true
			}));
		});
	}

	static get template() {
		return html([`<style>${ globalCss }\n${ css }</style> ${ template }`]);
	}
}
window.customElements.define('hub-tool-input-collection', HubToolInputCollection);
