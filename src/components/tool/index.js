'use strict';
/**
 * Attributes:
 *  @TODO
 *
 * Events:
 * 	'filesready': fires when the user selects files from the browser or drops them in the incoming area. doesn't imply they've been uploaded anywhere, but you can read data from evt.detail.files
 */

import { Element, html } from '@polymer/polymer/polymer-element';
import '@polymer/polymer/lib/elements/dom-repeat';
import msgpack from 'msgpack5';

import css from './style.pcss';
import template from './template.html';

import HubAPI from '../../lib/hub-api';

const { encode, decode } = msgpack();

export default class HubTool extends Element {
	static get properties() {
		return {
			'name': {
				'type': String
			},
			'hub': {
				'type': String,
				'value': () => location.host
			},
			'debug': {
				'type': Boolean,
				'value': false
			},
			'title': {
				'type': String
			}
		};
	}


	ready() {
		super.ready();
		const def = {
			'title': 'app-fermi',
			'inputs': [
				{
					'label': 'T',
					'unit': [ 'temperature', 'K' ],
					'type': [ 'number', 'decimal' ],
					'min': 0,
					'max': 500,
					'presets': [
						{
							'label': 'room temp',
							'default': true,
							'value': 300
						},
						{
							'label': 'liquid nitrogen',
							'value': 77
						},
						{
							'label': 'liquid helium',
							'value': 4.2
						}
					]
				},
				{
					'label': 'Ef',
					'unit': [ 'energy', '', 'eV' ],
					'type': [ 'number', 'decimal' ],
					'presets': [
						{
							'default': true,
							'value': 0
						}
					]
				}
			],
			'outputs': [
				{
					'label': 'Result',
					'path': '.',
					'type': '2d-coordinates'
				}
			]
		};
		this.title = def.title;
		const iface = definitionToElements(def);
		iface.inputs.forEach((inp) => this.$.inputs.appendChild(inp));
		return;
		this.sock = new WebSocket(`wss://${ this.hub }/`);
		this.sock.onopen = () => {
			this.sock.onmessage = (evt) => this._handleMessage(evt);
			this.sock.send(encode({
				'action': 'getToolDefinition',
				'service': this.tool
			}));
		};
		return;
		if (!this.name) {
			console.error('a tool name is required to instantiate hub-tool');
			return;
		}
		(new HubAPI(this.hub)).getRappture(this.name).then((def) => this.def = def);
	}

	_handleMessage(evt) {
		this._decodeMessage(evt.data).then(({ action, ...ctx }) => {
			console.log({ action, ctx });
		});
	}

	_decodeMessage(data) {
		return new Promise((resolve, reject) => {
			const fr = new FileReader;
			fr.onload = (evt) => resolve(decode(evt.target.result));
			fr.readAsArrayBuffer(data);
		});
	}


	static get template() {
		return html([`<style>${css}</style> ${template}`]);
	}
}
window.customElements.define('hub-tool', HubTool);

function definitionToElements(def) {
	const els = definitionToAbstractDom(def);
	const rv = { 'inputs': [], 'outputs': [] };
	els.inputs.forEach(([ comp, attrs ]) => {
		const el = document.createElement(comp);
		Object.entries(attrs).forEach(([ k, v ]) =>
			el.setAttribute(k, typeof v === 'object' ? JSON.stringify(v) : v)
		);

		const wrap = document.createElement('div');
		wrap.className = 'input-cont';
		wrap.appendChild(el);
		rv.inputs.push(wrap);
	});
	return rv;
}

function definitionToAbstractDom(def) {
	const rv = {
		'inputs': [],
		'outputs': []
	};

	if (def.inputs) {
		rv.inputs = def.inputs.map((inp) => [ 'hub-input', inp ])
	}
	return rv;
}
