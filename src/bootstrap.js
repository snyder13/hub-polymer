'use strict';
(function() {
	return;
	if (!('serviceWorker' in navigator)) {
		console.log('Service worker is not supported');
		return;
	}

	navigator.serviceWorker.register('/sw.js')
		.then((registration) => registration.onupdatefound = () => {
			if (navigator.serviceWorker.controller) {
				const installingWorker = registration.installing;
				installingWorker.onstatechange = () => {
					switch (installingWorker.state) {
						case 'installed': break;
						case 'redundant': throw new Error('The installing service worker became redundant.');
					}
				};
			}
		}).catch((e) => console.error('Error during service worker registration:', e));
})();

if (document.readyState === 'complete' || document.readyState === 'interactive') {
	ready();
}
else {
	document.addEventListener('DOMContentLoaded', ready);
}

function ready() {
	let base;
	Array.from(document.getElementsByTagName('script')).some((script) => {
		if (/\/bootstrap.js$/.test(script.src)) {
			base = script.src.replace(/bootstrap.js$/, '');
			return true;
		}
	});

	if (!base) {
		console.error('failed to determine base');
		return;
	}

	function writeScript(attrs) {
		const script = document.createElement('script');
		for (const k in attrs) {
			script.setAttribute(k, attrs[k]);
		}
		document.body.insertBefore(script, document.body.firstChild);
	}

	const fontcons = document.createElement('link');
	fontcons.setAttribute('href', base + 'assets/css/fontcons.css');
	fontcons.setAttribute('rel', 'stylesheet');
	document.body.insertBefore(fontcons, document.body.firstChild);

	writeScript({
		'src': base + 'vendor/custom-elements-es5-adapter.js',
		'async': true,
		'nomodule': true
	});
	writeScript({
		'src': base + 'vendor/webcomponents-loader.js',
		'async': true
	});

	document.addEventListener('WebComponentsReady', function componentsReady() {
		document.removeEventListener('WebComponentsReady', componentsReady, false);

		writeScript({
			'src': base + 'bundle.js',
			'async': true,
			'nomodule': true
		});

		writeScript({
			'src': base + 'module.bundle.js',
			'async': true,
			'type': 'module'
		});
	}, false);
}
