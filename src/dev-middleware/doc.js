'use strict';
const babel = require('babel-core');
const fs = require('fs');
const meta = require(`${ __dirname }/../lib/element-meta`);
const tpl = fs.readFileSync(`${ __dirname }/doc.html`, { 'encoding': 'utf8' });

module.exports = (app) => app.get('/', (req, res, next) => {
	res.type('text/html').send(
		tpl
			.replace('%META%', JSON.stringify(meta.list()).replace(/"/g, '&quot;'))
	);
});
