import { JupyterLabPlugin } from '@jupyterlab/application';
import { DocumentRegistry } from '@jupyterlab/docregistry';
import { INotebookModel, NotebookPanel } from '@jupyterlab/notebook';
import { IDisposable } from '@phosphor/disposable';
declare global {
    interface Window {
        'execPython': {
            'readyState': string;
            'exec': (code: string) => any;
            'ready': Promise<void>;
        } | null;
    }
}
export declare class ExecWidgetExtension implements DocumentRegistry.IWidgetExtension<NotebookPanel, INotebookModel> {
    createNew(nb: NotebookPanel, context: DocumentRegistry.IContext<INotebookModel>): IDisposable;
}
declare const extension: JupyterLabPlugin<void>;
export default extension;
