"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const disposable_1 = require("@phosphor/disposable");
class ExecWidgetExtension {
    createNew(nb, context) {
        if (window.execPython) {
            return;
        }
        window.execPython = {
            'readyState': 'waiting',
            'exec': null,
            'ready': new Promise((resolve) => {
                const wait = setInterval(() => {
                    if (!context.session.kernel || window.execPython.readyState === 'ready') {
                        return;
                    }
                    clearInterval(wait);
                    window.execPython.readyState = 'ready';
                    window.execPython.exec = (code) => context.session.kernel.requestExecute({ code }, true);
                    resolve();
                }, 50);
            })
        };
        // Usage elsewhere: execPython.ready.then(() => execPython.exec('x = 42').done.then(console.log, console.error))
        return new disposable_1.DisposableDelegate(() => { });
    }
}
exports.ExecWidgetExtension = ExecWidgetExtension;
const extension = {
    id: 'jupyterlab_hub_polymer',
    autoStart: true,
    activate: (app) => {
        console.log(app);
        app.docRegistry.addWidgetExtension('Notebook', new ExecWidgetExtension());
    }
};
exports.default = extension;
