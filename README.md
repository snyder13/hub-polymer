# hub-polymer
A set of web components designed for use in different contexts in the HUBzero platform.


## Prerequisites
  1. [node.js](https://nodejs.org/en/download) >= 10.0
  2. `yarn` >= 1.5 (`npm install -g yarn`)


## Installation
	$ git clone https://bitbucket.org/snyder13/hub-polymer
	$ cd hub-polymer
	$ yarn install


## Development Server

	$ PORT=3000 yarn run dev:module

This starts a server on `http://localhost:3000` with a bundle that is hot-reloaded when `src/` changes.

Navigating to this location will show documentation about the usage of the elements that are included. *(@TODO Where applicable -- I haven't annotated most of the elements yet)*

You can also view the component usage document here: https://doc.rmer.info/dist


## Production

	$ yarn build:prod

This makes a more robust bundle with polyfills for browser support in the `dist/` path. This path can be served statically with Apache or Nginx.


## Writing Elements

There is a [walkthrough showing the building a few simple elements](doc/example-elements.md).


## Jupyter Integration

Plugins are included for both Jupyter Notebooks and JupyterLab. Run whichever installer you like, or both:

	$ yarn lab-plugin
	$ yarn notebook-plugin

Depending on where Jupyter is installed you may need `sudo` for these. You can define `JUPYTER_BIN` to specify which Jupyter executable to use. For example, I have a dev version that is different from the system install:

	$ JUPYTER_BIN=~/.local/bin/jupyter yarn lab-plugin

These plugins enable some behaviors based on convention:

 * If an element has its `id` attribute set, and it has a `value` attribute, that value will be persisted to a Python variable with the name given by `id`. `<hub-foo id="bar" value="42"></hub-foo>` would result in `print(bar) # 42` in Python-land. For dynamic values, emitting a `change` even with `detail.value` will update the binding.
 * On the other side of the binding, if an element has its `data-bind` attribute set, if a variable by the given name exists in Python its value will be assigned to the element's value. This can be used to connect to another element by its `id` or to use any arbitrary Python variable.

The walkthrough above demonstrates both of these behaviors in more detail.

You can also implement custom behaviors through the abstraction in `src/lib/notebook.js`, which behaves the same for both notebooks and labs:

```javascript
import Notebook from 'lib/notebook';
// promise that resolves when the kernel is available. never resolves when run on a page w/o Jupyter
Notebook.ready.then(() => {
	// run some Python code. resolves with the resulting `stdout`, or rejects with a description of an error
	Notebook.exec('print(42)')
		.then(console.log, console.error);
});
```

There is an example of extending Jupyter interactions like this in `src/lib/upload-strategies.js`, used by `<hub-input-file>`

## Motivation

Web components are an evolving standard, but they're slated to be part of how browsers will work by default in the future. Polymer is a polyfill that allows us to target that model early.

The chief advantage of web components is encapsulation -- they form their own private document across which styles and markup does not leak. Data flows through properties on the element and through familiar JavaScript events like `onchange`. Because of this encapsulation the elements can be re-used anywhere with confidence that they perform the same. My goal is to situate us so that when we common visualizations and controls that are applicable to a lot of different projects we can maintain use the same component for all of them, presenting a uniform experience and eliminating redundant effort.

Another advantage is the declarative nature of the use of Polymer elements. Since we are able to construct elements such that they are used entirely by writing out HTML tags and attributes, they can be used by end-users to achieve dynamic results even with a content-security-policy that restricts from directly using any JavaScript. For example, I've done some proof-of-concept work that woudl allow a user to embed something like `<hub-tool name="cntbands" param-x="42" ...><hub-viz-3d-structure></hub-viz-3d-structure></hub-tool>` in a blog post, embedding illustrative tool output along with the rest of their text.

If you are partial to React or Vue or etc., fear not, as they are not contraindicated by Polymer, which isn't itself really a framework. From the outside, a Polymer element appears as though it is any other HTML tag, so it can be managed just as well by the template language of one of these frameworks. Likewise, from the inside the element appears as a DOM document like any web page, so elements can also embed arbitrary client frameworks and libraries. (The example above embeds D3)
