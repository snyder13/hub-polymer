'use strict';
// replaced by BOOTSTRAP_URL in env when running `yarn build`

const BOOTSTRAP_URL = 'http://localhost:3000/bootstrap.js';

class ExecWidgetExtension {
	createNew(_notebook, context) {
		if (window._hubExecPython) {
			return;
		}
		const iface = window._hubExecPython = {
			'readyState': 'waiting',
			'exec': null,
			'ready': new Promise(resolve => {
				const wait = setInterval(() => {
					if (!context.session.kernel || window._hubExecPython.readyState === 'ready') {
						return;
					}
					clearInterval(wait);
					window._hubExecPython.readyState = 'ready';
					window._hubExecPython.exec = code => {
						const future = context.session.kernel.requestExecute({ code });
						let out = null,
						    isError = false;
						future.onIOPub = ({ msg_type, content }) => {
							console.log({ msg_type, content });
							if (msg_type === 'error') {
								isError = true;
								out = content;
							} else if (msg_type === 'stream') {
								out = content.text;
							} else if (msg_type === 'execute_result') {
								out = content.data['text/plain'];
							}
						};
						future.output = future.done.then(() => {
							console.log('resolve output');
							if (isError) {
								throw out;
							}
							return out;
						});

						return future;
					};
					resolve(window._hubExecPython.exec);
				}, 50);
			})
		};
	}
	// Usage elsewhere:
	// _hubExecPython.ready
	// 	.then((exec) => exec('print(42)'))
	// 	.then((res) =>
	// 		res.done.then(console.log, console.error)
	// 		res.output.then(console.log, console.error)
	// 	)
}

module.exports = {
	'id': 'jupyterlab_hub_polymer',
	'autoStart': true,
	'activate': app => {
		const script = document.createElement('script');
		script.src = BOOTSTRAP_URL;
		if (document.body !== null) {
			document.body.appendChild(script);
		}

		app.docRegistry.addWidgetExtension('Notebook', new ExecWidgetExtension());
	}
};