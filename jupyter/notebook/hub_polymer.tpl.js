'use strict';
// replaced by BOOTSTRAP_URL in env when running `yarn notebook-plugin`
const BOOTSTRAP_URL = '%BOOTSTRAP_URL%';

define(() => {
	return {
		'load_ipython_extension': () => {
			console.log('hub-polymer');
			const script = document.createElement('script');
			script.src = BOOTSTRAP_URL;
			if (document.body !== null) {
				document.body.appendChild(script);
			}
		}
	};
});
