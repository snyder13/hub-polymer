'use strict';
const meta = require(`${ __dirname }/../src/lib/element-meta`);
const tpl = require('fs').readFileSync(`${ __dirname }/../src/dev-middleware/doc.html`, { 'encoding': 'utf8' });
console.log(tpl.replace('%META%', JSON.stringify(meta.list()).replace(/"/g, '&quot;')));
