'use strict';
const path     = require('path');
const fs       = require('fs');
const readline = require('readline');

if (!process.argv[2]) {
	console.error(`Usage: ${ process.argv.slice(0, 2).map((p) => path.basename(p)).join(' ') } relative/path/to/new/component (relative to src/components)`);
	process.exit(1);
}

const base = path.resolve(`${ __dirname }/../src/components`);
const parts = process.argv[2].replace(/^\/|[.]js/g, '').split('/');
const srcRel = (new Array(parts.length + 1)).fill('..').join('/');
const names = {
	'element': 'hub-' + parts.join('-'),
	// [ some, thing ] -> SomeThing
	'class'  : 'Hub' + parts.map((part) =>
		// some-thing -> SomeThing
		// something -> Something
		part
			.split('-')
			.map((part) => `${ part[0].toUpperCase() }${ part.slice(1) }`)
			.join('')
	).join('')
};

let dest = base;
parts.forEach((part) => {
	dest += `/${ part }`;
	if (!fs.existsSync(dest)) {
		fs.mkdirSync(dest);
	}
});

function copyDir(dir, dest) {
	fs.readdirSync(dir).filter((file) => !/^[.]/.test(file)).forEach((file) => {
		const destFile = file.replace('%ELEMENT_NAME%', names.element);
		console.log(`${ dest.replace(/^.*src\//, 'src/') }/${ destFile }`);
		if (fs.existsSync(`${ dest }/${ file }`)) {
			console.log('\texists, skipping');
			return;
		}
		if (fs.statSync(`${ dir }/${ file }`).isDirectory()) {
			if (!fs.existsSync(`${ dest }/${ file }`)) {
				fs.mkdirSync(`${ dest }/${ file }`);
			}
			copyDir(`${ dir }/${ file }`, `${ dest }/${ file }`);
			return;
		}
		const content = fs.readFileSync(`${ dir }/${ file }`, { 'encoding': 'utf8' });
		fs.writeFileSync(
			`${ dest }/${ destFile }`,
			content
				.replace(/%CLASS_NAME%/g, names.class)
				.replace(/%ELEMENT_NAME%/g, names.element)
				.replace(/%SRC_REL%/g, srcRel)
		);
	});
}
copyDir(`${ base }/../skeleton`, dest);

const indexLines = {};
const index = fs.readFileSync(`${ base }/../index.js`, { 'encoding': 'utf8' });
index
	.split(/[\r\n]+/)
	.forEach((line) => indexLines[line] = true);
const imp = `import './components/${ parts.join('/') }';`;
if (indexLines[imp]) {
	console.log('already in src/index.js');
}
else {
	fs.writeFileSync(`${ base }/../index.js`, `${ index.replace(/[\r\n]+$/, '') }\n${ imp }\n`);
	console.log('added import to src/index.js');
}
